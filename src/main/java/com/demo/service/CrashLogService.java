package com.demo.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.dao.CrashLogDao;
import com.demo.domain.CrashLog;
import com.google.common.collect.Lists;

@Service("crashLogService")
public class CrashLogService {
	
	@Autowired
	CrashLogDao crashLogDao;
	
	/**
	 * 添加日志
	 * @param log
	 * @return
	 */
	public boolean addCrash(CrashLog log){
		if(crashLogDao.add(log) != null){
			return true;
		}
		return false;
	}
	
	/**
	 * 删除记录
	 * @param ids 用，分开  列如：1,2,3
	 * @return
	 */
	public boolean deleteCrash(String ids){
		String[] arr = ids.split(",");
		List<Integer> idList = Lists.newArrayList();
		for (String string : arr) {
			idList.add(Integer.parseInt(string));
		}
		return crashLogDao.delByIds(idList);
	}
	
	/**
	 * 查询记录
	 * @return
	 */
	public Map<String, Object> selectCrash(int appType, int page, int pageSize){
		Map<String, Object> map = new HashMap<>();
		map.put("list", crashLogDao.getByPage(appType, page, pageSize));
		map.put("count", crashLogDao.getCountByType(appType));
		return map;
	}
}
