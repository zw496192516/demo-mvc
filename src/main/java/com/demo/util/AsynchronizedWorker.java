package com.demo.util;

import java.util.concurrent.ThreadPoolExecutor;

/**
 * 异步工作队列服务器

 *
 * 2014-5-5下午05:47:52
 */
public class AsynchronizedWorker {
	private ThreadPoolExecutor taskExecutor;

	public void setTaskExecutor(ThreadPoolExecutor taskExecutor) {
		this.taskExecutor = taskExecutor;
	}
	
	public void exec(Runnable run){
		taskExecutor.execute(run);
	}
	
}
