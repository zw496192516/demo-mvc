package com.demo.dao.impl;

import java.util.List;
import java.util.Map;

import org.mybatis.spring.support.SqlSessionDaoSupport;

import com.demo.dao.CrashLogDao;
import com.demo.domain.CrashLog;
import com.google.common.collect.Maps;

public class CrashLogDaoImpl extends SqlSessionDaoSupport implements CrashLogDao {

	@Override
	public CrashLog add(CrashLog c) {
		int num = getSqlSession().insert("com.demo.dao.CrashLogDao.add", c);
		return num > 0 ? c : null;
	}

	@Override
	public List<CrashLog> getByTypeCode(int appType, int errorCode, int page, int pageSize) {
		int start = (page - 1) * pageSize;
		start = Math.max(start, 0);
		
		Map<String, Object> param = Maps.newTreeMap();
		param.put("appType", appType);
		param.put("errorCode", errorCode);
		param.put("start", start);
		param.put("end", pageSize);
		
		return getSqlSession().selectList("com.demo.dao.CrashLogDao.getByTypeCode", param);
	}

	@Override
	public List<CrashLog> getByPage(int appType, int page, int pageSize) {
		int start = (page - 1) * pageSize;
		start = Math.max(start, 0);
		
		Map<String, Object> param = Maps.newTreeMap();
		param.put("appType", appType);
		param.put("start", start);
		param.put("end", pageSize);
		
		return getSqlSession().selectList("com.demo.dao.CrashLogDao.getByPage", param);
	}

	@Override
	public boolean update(int appType, int errorCode, int status) {
		Map<String, Object> param = Maps.newTreeMap();
		param.put("appType", appType);
		param.put("errorCode", errorCode);
		param.put("status", status);
		return getSqlSession().update("com.demo.dao.CrashLogDao.update", param) > 0;
	}

	@Override
	public int getCountByType(int appType) {
		return getSqlSession().selectOne("com.demo.dao.CrashLogDao.getCountByType", appType);
	}

	@Override
	public boolean delByIds(List<Integer> ids) {
		return getSqlSession().delete("com.demo.dao.CrashLogDao.delByIds", ids) > 0;
	}

}

