package com.demo.dao;

import java.util.List;

import com.demo.domain.CrashLog;

/**
 * 奔溃日志
 */
public interface CrashLogDao {
	
	/**
	 * 添加日志
	 * @param c
	 * @return
	 */
	public CrashLog add(CrashLog c);
	
	/**
	 * 获取指定产品的指定错误
	 * @param appType
	 * @param error_code
	 * @return
	 */
	public List<CrashLog> getByTypeCode(int appType, int errorCode, int page, int pageSize);
	
	/**
	 * 分页获取产品奔溃日志
	 * @param appType
	 * @return
	 */
	public List<CrashLog> getByPage(int appType, int page, int pageSize);
	
	/**
	 * 更新某类型的错误日志状态
	 * @param appType
	 * @param error_code
	 * @param status
	 * @return
	 */
	public boolean update(int appType, int errorCode, int status);
	
	/**
	 * 根据类型获取奔溃日志数量
	 * @param appType
	 * @return
	 */
	public int getCountByType (int appType);
	
	/**
	 * 批量删除崩溃日志
	 * @param ids
	 * @return
	 */
	public boolean delByIds (List<Integer> ids);
}
