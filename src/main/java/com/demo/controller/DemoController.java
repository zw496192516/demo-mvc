package com.demo.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.demo.dao.CrashLogDao;
import com.demo.domain.CrashLog;
import com.demo.service.CrashLogService;
import com.google.common.collect.ImmutableMap;

@Controller
public class DemoController {
	
	@Autowired
	CrashLogService crashLogService;
	@Autowired
	CrashLogDao crashLogDao;
	
	/**
	 * 测试使用
	 * @return
	 */
	@RequestMapping(value = "/web/test.x", produces = {"application/json"})
	public @ResponseBody Object test(){
		
		return "测试1232";
	}
	
	/**
	 * 添加一条crash日志
	 * @param request
	 * @param type app类型：0表示ios版爱车通，1表示android版爱车通
	 * @param version app版本号
	 * @param errorCode 错误码，由客户端自己定义
	 * @param deviceInfo 设备类型：如iPhone5c，ios8.1.1，未越狱、小米3，miui3，android4.4等
	 * @param content 日志内容
	 * @return
	 */
	@RequestMapping(value = "/addCrashlog.x", produces = {"application/json; charset=UTF-8"})
	public @ResponseBody Object addCrashlog(HttpServletRequest request, int type, String version, int errorCode, String deviceInfo, String content){
		
		CrashLog log = new CrashLog();
		log.setApp_type(type);
		log.setApp_version(version);
		log.setError_code(errorCode);
		log.setDevice_info(deviceInfo);
		log.setContent(content);
		
		if(crashLogService.addCrash(log)){
			return ImmutableMap.of("result", "1" , "info", "添加成功！");
		}
		return ImmutableMap.of("result", "0" , "info", "添加失败！");
	}
	
	/**
	 * 删除多条crash日志
	 * @param request
	 * @param ids 用，分开  列如：1,2,3
	 * @return
	 */
	@RequestMapping(value = "/deleteCrashlog.x", produces = {"application/json; charset=UTF-8"})
	public @ResponseBody Object deleteCrashlog(HttpServletRequest request, String ids){
		
		if(crashLogService.deleteCrash(ids)){
			return ImmutableMap.of("result", "1" , "info", "删除成功！");
		}
		return ImmutableMap.of("result", "0" , "info", "删除失败！");
	}
	
	/**
	 * 查询crash日志
	 * @param request
	 * appType 等于1 有值
	 * 事列：http://localhost:8080/demo-mvc/selectCrashlog.x?appType=1&page=1&pageSize=10
	 * @return
	 */
	@RequestMapping(value = "/selectCrashlog.x", produces = {"application/json; charset=UTF-8"})
	public @ResponseBody Object selectCrashlog(HttpServletRequest request, int appType, int page, int pageSize){
		
		return ImmutableMap.of("result", 1, "info", crashLogService.selectCrash(appType, page, pageSize));
	}
}
