package com.demo.domain;

import java.io.Serializable;

/**
 * 汽车图片实体类
 * @author zhuwei
 *
 */
/**
 * @author zhuwei
 *
 */
public class CarImage implements Serializable{

	private static final long serialVersionUID = -2354514104097241591L;

	private int id;	
	private int car_detail_id;			/*对应具体车系ID（关联表t_car_detail）*/
	private int color_id;				/*关联颜色表id*/
	private int image_of_part_type;		/*图片类型（1外观，2内饰，3空间，4图解，5官方图车体颜色图，6官方详细图，7车展）*/
	private String desc;				/*图片文字描述*/
	private String url;					/*图片路径*/
	private String yi_che_url;				/* 易车URL地址 */
	private int status;					/*删除字段（0.代表未删除 1.代表删除）*/
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getCar_detail_id() {
		return car_detail_id;
	}
	public void setCar_detail_id(int car_detail_id) {
		this.car_detail_id = car_detail_id;
	}
	public int getColor_id() {
		return color_id;
	}
	public void setColor_id(int color_id) {
		this.color_id = color_id;
	}
	public int getImage_of_part_type() {
		return image_of_part_type;
	}
	public void setImage_of_part_type(int image_of_part_type) {
		this.image_of_part_type = image_of_part_type;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getYi_che_url() {
		return yi_che_url;
	}
	public void setYi_che_url(String yi_che_url) {
		this.yi_che_url = yi_che_url;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
}
