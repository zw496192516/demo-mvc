package com.demo.domain;

import java.io.Serializable;

/**
 * 汽车详细资料
 * @author zhuwei
 *
 */
public class CarDetail implements Serializable {

	private static final long serialVersionUID = -479684178075753649L;
	private int id;
	  private int cpp_detail_id; 	/*汽车系列id（对应表t_car_pin_pai_detail表）*/		/* 2 、有    1、选配   0、无    -1、为空 */
	  private int dang_shu;			/*变速箱的档数*/
	  private String shang_jia_bao_jia;  	/*商家报价*/
	  private int jiang_jia_you_hui;   		/*降价优惠*/
	  private String bao_xiu_zheng_ce;  	/*保修政策*/
	  private String che_chuan_shui_jian_mian;  	/*车船税减免*/
	  private float shi_qu_gong_kuang_you_hao;  	/*市区工况油耗(L/100km)*/
	  private float shi_jiao_gong_kuang_you_hao;  	/*市郊工况油耗(L/100km)*/
	  private float zong_he_gong_kuang_you_hao; 	/*综合工况油耗(L/100km)*/
	  private float wang_you_you_hao;  				/*网友油耗(L/100km)*/
	  private int zui_gao_che_su;   				/*最高车速(km/h)*/
	  private String che_shen_yan_se; 				/*车身颜色*/
	  private int che_chang;   		/*车长(mm)*/
	  private int che_kuan;  		/*车宽(mm)*/
	  private int che_gao;  		/*车高(mm)*/
	  private int zhou_ju;  		/*轴距(mm)*/
	  private int qian_lun_ju;  	/*前轮距(mm)*/
	  private int hou_lun_ju;  		/*后轮距(mm)*/
	  private float yi_che_shi_ce_you_hao;			/*易车实测油耗(L/100km)*/
	  private float yi_che_shi_ce_gong_li_jia_su;	/*易车实测0-100公里加速时间(s)*/
	  private int zheng_bei_zhi_liang;  	/*整备质量(kg)*/
	  private int man_zai_zhi_liang;  		/*满载质量(kg)*/
	  private float zui_xiao_li_di_jian_xi;  	/*最小离地间隙(mm)*/
	  private String jie_jin_jiao;  		/*接近角(mm)*/
	  private String tong_guo_jiao;  		/*通过角(mm)*/
	  private String li_qu_jiao;  			/*离去角(mm)*/
	  private int xing_li_xiang_rong_ji;	/*行李厢容积(L)*/
	  private int xing_li_xiang_zui_da_kuo_zhan_rong_ji;/*行李厢最大拓展容积(L)*/
	  private int xing_li_xiang_gai_kai_he; /*行李厢盖开合方式(1. 手动、2.自动)*/
	  private String xing_li_xiang_da_kai_fang_shi; /*行李厢打开方式*/
	  private String che_ding_xing_shi;  			/*车顶型式*/
	  private String che_peng_xing_shi; 			/*车篷型式*/
	  private int che_ding_xing_li_xiang_jia;  		/*车顶行李厢架*/
	  private int hou_dao_liu_wei_yi;  				/*后导流尾翼*/
	  private int yun_dong_bao_wei;  				/*运动包围*/
	  private String fa_dong_ji_wei_zhi;  			/*发动机位置*/
	  private String fa_dong_ji_xing_hao;  			/*发动机型号*/
	  private int pai_liang_hao_sheng;				/*排量(mL)*/
	  private String jin_qi_xing_shi;  				/*进气形式*/
	  private String qi_gang_pai_lie_xing_shi;  	/*气缸排列形式*/
	  private int qi_gang_shu;   					/*汽缸数(个)*/
	  private int mei_gang_qi_men_shu;  			/*每缸气门数(个)*/
	  private String qi_men_jie_gou;  				/*气门结构*/
	  private String ya_suo_bi;  					/*压缩比*/
	  private String gang_jing;  					/*缸径(mm)*/
	  private String xing_cheng;  					/*行程(mm)*/
	  private float zui_da_ma_li;   				/*最大马力(ps)*/
	  private float zui_da_gong_lv;   				/*最大功率(KW)*/
	  private String zui_da_gong_lv_zhuan_su;   	/*最大功率转速(rpm)*/
	  private float zui_da_niu_ju;  				/*最大扭矩(N.M)*/
	  private String zui_da_niu_ju_zhuan_su;  		/*最大扭矩转速(rpm)*/
	  private String te_you_ji_shu;  				/*特有技术*/
	  private String ran_you_biao_hao;				/* 燃料标号 */
	  private String gong_you_fang_shi;  			/*供油方式*/
	  private float ran_you_xiang_rong_ji;  		/*燃油箱容积(L)*/
	  private String gang_gai_cai_liao;  			/*缸盖材料*/
	  private String gang_ti_cai_liao;  			/*缸体材料*/
	  private String huan_bao_biao_zhun;  			/*环保标准*/
	  private int qi_dong_xi_tong;					/*启动系统*/
	  private int huan_dang_bo_pian;				/*换挡拨片*/
	  private float dian_dong_zui_da_gong_lv;		/*电机最大功率(kW)*/
	  private float dian_ji_e_ding_gong_lv;			/*电机额定功率(kW)*/
	  private String che_ti_jie_gou;  				/*车体结构*/
	  private float zui_xiao_zhuan_wan_ban_jing;  	/*最小转弯半径(m)*/
	  private String zhuan_xiang_zhu_li;  			/*转向助力*/
	  private String qian_zhi_dong_lei_xing;  		/*前制动类型*/
	  private String hou_zhi_dong_lei_xing;  		/*后制动类型*/
	  private String zhu_che_zhi_dong_lei_xing; 	/*驻车制动类型*/
	  private int kong_qi_xian_gua; 				/*空气悬挂*/
	  private int ke_tiao_xuan_gua;					/*可调悬挂*/
	  private String qian_xuan_gua_lei_xing;  		/*前悬挂类型*/
	  private String hou_xuan_gua_lei_xing;  		/*后悬挂类型*/
	  private int jia_shi_wei_an_quan_qi_nang;   	/*驾驶位安全气囊*/
	  private int fu_jia_shi_wei_an_quan_qi_nang;   /*副驾驶位安全气囊*/
	  private int qian_pai_ce_an_quan_qi_nang;   	/*前排侧安全气囊*/
	  private int qian_pai_tou_bu_qi_nang;   		/*前排头部气囊(气帘)*/
	  private int xi_bu_qi_nang;   					/*膝部气囊*/
	  private int hou_pai_ce_an_quan_qi_nang;   	/*后排侧安全气囊*/
	  private int hou_pai_tou_bu_qi_nang;   		/*后排头部气囊(气帘)*/
	  private int an_quan_dai_wei_xi_ti_shi;   		/*安全带未系提示*/
	  private int an_quan_dai_xian_li_gong_neng;   	/*安全带限力功能*/
	  private int an_quan_dai_yu_shou_jin_gong_neng;/*安全带预收紧功能*/
	  private String qian_an_quan_dai_tiao_jie;  	/*前安全带调节*/
	  private int hou_pai_an_quan_dai;   			/*后排安全带*/
	  private int hou_pai_zhong_jian_san_dian;   	/*后排中间三点式安全带*/
	  private int tai_ya_jian_ce_zhuang_zhi;   		/*胎压监测装置*/
	  private int ling_ya_xu_xing;   				/*零压续行(零胎压继续行驶)*/
	  private int ke_kui_suo_zhuan_xiang_zhu;  	 	/*可溃缩转向柱*/
	  private int che_nei_zhong_kong_suo;   		/*车内中控锁*/
	  private int zhong_kong_men_suo;   			/*中控门锁*/
	  private int er_tong_suo;   					/*儿童锁*/
	  private int yao_kong_yao_shi;   				/*遥控钥匙*/
	  private int wu_yao_shi_qi_dong_xi_tong;   	/*无钥匙启动系统*/
	  private int fa_dong_ji_dian_zi_fang_dao;   	/*发动机电子防盗*/
	  private String qian_lun_tai_gui_ge;  			/*前轮胎规格*/
	  private String hou_lun_tai_gui_ge;  			/*后轮胎规格*/
	  private String qian_lun_gu_gui_ge;  			/*前轮毂规格*/
	  private String hou_lun_gu_gui_ge;  			/*后轮毂规格*/
	  private String bei_tai_lei_xing;  			/*备胎类型*/
	  private String lun_gu_cai_liao;  				/*轮毂材料*/
	  private int sha_che_fang_bao_si;   			/*刹车防抱死(ABS)*/
	  private int dian_zi_zhi_dong_li_fen_pei_xi_tong;   /*电子制动力分配系统(EBD)*/
	  private int sha_che_fu_zhu;  					 /*刹车辅助(EBA/BAS/BA/EVA等)*/
	  private int qian_yin_li_kong_zhi;  			 /*牵引力控制(ASR/TCS/TRC/ATC等)*/
	  private int dong_tai_wen_ding_kong_zhi_xi_tong;    /*动态稳定控制系统(ESP)*/
	  private int sui_su_zhu_li_zhuan_xiang_tiao_jie;    /*随速助力转向调节(EPS)*/
	  private int zi_dong_zhu_che;  			 	/*自动驻车*/
	  private int shang_po_fu_zhu;   				/*上坡辅助*/
	  private int dou_po_huan_jiang;   				/*陡坡缓降*/
	  private int bo_che_lei_da;   					/*泊车雷达(车前)*/
	  private int dao_che_lei_da;   				/*倒车雷达(车后)*/
	  private int dao_che_ying_xiang;   			/*倒车影像*/
	  private int quan_jing_she_xiang_tou;   		/*全景摄像头*/
	  private int ding_su_xun_hang;  			/*定速巡航*/
	  private int zi_shi_ying_xun_hang;   			/*自适应巡航*/
	  private int gps_dao_hang;   					/*GPS导航系统*/
	  private int ren_ji_jiao_hu_xi_tong;   		/*人机交互系统*/
	  private int zi_dong_bo_che_ru_wei;   			/*自动泊车入位*/
	  private int bing_xian_fu_zhu;   				/*并线辅助*/
	  private int zhu_dong_sha_che;   				/*主动刹车*/
	  private int zheng_ti_zhu_dong_zhuan_xiang_xi_tong;   /*整体主动转向系统*/
	  private int ye_shi_xi_tong;   				/*夜视系统*/
	  private String kai_men_fang_shi;  			/*开门方式*/
	  private String dian_dong_che_chuang;  		/*电动车窗*/
	  private String fang_zi_wai_xian;  			/*防紫外线/隔热玻璃*/
	  private String dian_dong_chuang_fang_jia;  	/*电动窗防夹功能*/
	  private String tian_chuang_kai_he_fang_shi;  	/*天窗开合方式*/
	  private String tian_chuang_xing_shi;   		/*天窗型式*/
	  private int hou_chuang_zhe_yang_lian;   		/*后窗遮阳帘*/
	  private int hou_pai_ce_zhe_yang_lian;   		/*后排侧遮阳帘*/
	  private int hou_yu_shua_qi;   				/*后雨刷器*/
	  private int gan_ying_yu_shua;   				/*感应雨刷*/
	  private int dian_dong_xi_he_men;   			/*电动吸合门*/
	  private int xing_li_xiang_dian_dong_xi_he_men; 			/*行李厢电动吸合门*/
	  private int hou_shi_jing_dai_ce_zhuan_xiang_deng;   		/*后视镜带侧转向灯*/
	  private int wai_hou_shi_jing_ji_yi_gong_neng; 			/*外后视镜记忆功能*/
	  private int wai_hou_shi_jing_jia_re_gong_neng;			/*外后视镜加热功能*/
	  private int wai_hou_shi_jing_dian_dong_zhe_die_gong_neng; /*外后视镜电动折叠功能*/
	  private int wai_hou_shi_jing_dian_dong_tiao_jie;   		/*外后视镜电动调节*/
	  private int nei_hou_shi_jing_fang_xuan_mu;   				/*内后视镜防眩目功能*/
	  private int zhe_yang_ban_hua_zhuang_jing;   				/*遮阳板化妆镜*/
	  private String qian_zhao_deng_lei_xing;  					/*前照灯类型*/
	  private int qian_da_deng_zi_dong_kai_bi;   				/*前大灯自动开闭*/
	  private int qian_zhao_deng_zi_dong_qing_xi;   			/*前照灯自动清洗功能*/
	  private int qian_da_deng_yan_shi_guan_bi;					/*前大灯延时关闭*/
	  private int qian_da_deng_sui_dong_zhuan_xiang;			/*前大灯随动转向*/
	  private int qian_zhao_deng_zhao_she_fan_wei_tiao_zheng;   /*前照灯照射范围调整*/
	  private int hui_che_qian_deng_fang_xuan_mu_gong_neng;  	/*会车前灯防眩目功能*/
	  private int qian_wu_deng;   				/*前雾灯*/
	  private int che_xiang_qian_yue_du_deng;   /*车厢前阅读灯*/
	  private int che_xiang_hou_yue_du_deng;    /*车厢后阅读灯*/
	  private int che_nei_fen_wei_deng;   		/*车内氛围灯*/
	  private int ri_jian_xing_che_deng;   		/*日间行车灯*/
	  private int led_wei_deng;					/*LED尾灯*/
	  private int gao_wei_zhi_dong_deng;   		/*高位（第三）制作灯*/
	  private int zhuan_xiang_tou_deng;   		/*转向头灯(辅助灯)*/
	  private String ce_zhuan_xiang_deng;  		/*侧转向灯*/
	  private String xing_li_xiang_deng;  		/*行李厢灯*/
	  private int fang_xiang_pan_qian_hou_tiao_jie;  	/*方向盘前后调节*/
	  private int fang_xiang_pan_shang_xia_tiao_jie; 	/*方向盘上下调节*/
	  private String fang_xiang_pan_tiao_jie_fang_shi;  	/*方向盘调节方式*/
	  private int fang_xiang_pan_ji_yi_she_zhi;				/*方向盘记忆设置*/
	  private String fang_xiang_pan_biao_mian_cai_liao;  	/*方向盘表面材料*/
	  private int duo_gong_neng_fang_xiang_pan;   			/*多动能方向盘*/
	  private String duo_gong_neng_fang_xiang_pan_gong_neng;/*多功能方向盘功能*/
	  private int xing_che_dian_nao_xian_shi_ping;   		/*行车电脑显示屏*/
	  private String che_nei_dian_yuan_dian_ya;				/*车内电源电压*/
	  private int hud_tai_tou_shu_zi_xian_shi;   			/*HUD抬头数字显示*/
	  private String nei_shi_yan_se;  			/*内饰颜色*/
	  private int hou_pai_bei_jia;   			/*后排杯架*/
	  private int yun_dong_zuo_yi;   			/*运动座椅*/
	  private String zuo_yi_cai_liao;  			/*座椅材料*/
	  private String jia_shi_zuo_zuo_yi_tiao_jie_fang_shi;  	/*驾驶座座椅调节方式*/
	  private String jia_shi_zuo_zuo_yi_tiao_jie_fang_xiang; 	/*驾驶座座椅调节方向*/
	  private String fu_jia_shi_zuo_yi_tiao_jie_fang_shi;  		/*副驾驶座椅调节方式*/
	  private String fu_jia_shi_zuo_yi_tiao_jie_fang_xiang;  	/*副驾驶座椅调节方向*/
	  private String jia_shi_zuo_yao_bu_zhi_cheng_tiao_jie;  	/*驾驶座腰部支撑调节*/
	  private String jia_shi_zuo_jian_bu_zhi_cheng_tiao_jie;  	/*驾驶座肩部支撑调节*/
	  private String qian_zuo_yi_tou_zhen_tiao_jie;  	/*前座椅头枕调节*/
	  private String hou_pai_zuo_yi_tiao_jie_fang_shi;  /*后排座椅调节方式*/
	  private String hou_pai_zuo_wei_fang_dao_bi_li;  	/*后排座位放倒比例*/
	  private String qian_zuo_zhong_yang_fu_shou;  		/*前座中央扶手*/
	  private String hou_zuo_zhong_yang_fu_shou;  		/*后座中央扶手*/
	  private String zuo_yi_tong_feng;  				/*座椅通风*/
	  private String zuo_yi_jia_re;  					/*座椅加热*/
	  private String zuo_yi_an_mo_gong_neng;  			/*座椅按摩功能*/
	  private String dian_dong_zuo_yi_ji_yi;  			/*电动座椅记忆*/
	  private String er_tong_an_quan_zuo_yi;  			/*儿童安全座椅固定装置*/
	  private String di_san_pai_zuo_yi;  				/*第三排座椅*/
	  private String che_zai_dian_hua;  				/*车载电话*/
	  private String lan_ya_xi_tong;  					/*蓝牙系统*/
	  private String wai_jie_yin_yuan_jie_kou;  		/*外接音源接口*/
	  private String test_nei_zhi_ying_pan;             /*内置硬盘与车载电视之间没有显示的属性*/
	  private String nei_zhi_ying_pan;  				/*内置硬盘*/
	  private String che_zai_dian_shi;  				/*车载电视*/
	  private int yang_sheng_qi_shu_liang;   			/*扬声器数量*/
	  private String DVD;  								/*DVD*/
	  private String test_DVD;                          /*DVD与CD之间不显示的属性*/
	  private String test_DVD1;                         /*DVD与CD之间不显示的属性1*/
	  private String CD_stauts;                         /*CD状态*/
	  private String CD_num;							/*CD碟数量*/
	  private String zhong_kong_tai_ye_jing_ping;  		/*中控台液晶屏*/
	  private String hou_pai_ye_jing_ping;  			/*后排液晶屏*/
	  private String jia_su_shi_jian;					/*加速时间*/
	  private String zhi_dong_ju_li;					/*制动距离*/
	  private String you_hao;							/*油耗*/
	  private String rao_zhuang_su_du;					/*绕桩速度*/
	  private String che_nei_dai_su_zao_yin;			/*车内怠速噪音*/
	  private String che_nei_deng_su_zao_yin40;			/*车内等速（40km/h）噪音(dB)*/
	  private String che_nei_deng_su_zao_yin60;			/*车内等速（60km/h）噪音(dB)*/
	  private String che_nei_deng_su_zao_yin80;			/*车内等速（80km/h）噪音(dB)*/
	  private String che_nei_deng_su_zao_yin100;		/*车内等速（100km/h）噪音(dB)*/
	  private String che_nei_deng_su_zao_yin120;		/*车内等速（120km/h）噪音(dB)*/
	  private String kong_tiao_kong_zhi_fang_shi;  		/*空调控制方式*/
	  private String wen_du_fen_qu_kong_zhi;  			/*温度分区控制*/
	  private int wen_du_fen_qu_kong_zhi_num;			/*温度分区控制数量*/
	  private String hou_pai_du_li_kong_tiao;  			/*后排独立空调*/
	  private String hou_pai_chu_feng_kou;  			/*后排出风口*/
	  private String kong_qi_tiao_jie; 					/*空气调节/花粉过滤*/
	  private String kong_qi_jing_hua_zhuang_zhi;       /*车内空气净化装置*/
	  private String che_zai_bing_xiang;  				/*车载冰箱*/
	  
	  private String guan_fang_jia_su_shi_jian;			/*官方0-100公里加速时间(s)*/
	  private String zui_da_she_shui_shen_du;			/*最大涉水深度(mm)*/
	  private String feng_zu_xi_shu;					/*风阻系数*/
	  private String test3_zhu;							/*未知数据*/
	  private String test6_zhu;							/*未知数据*/
	  private String test7_zhu;							/*未知数据*/
	  private int dian_zi_dang_gan;						/*电子档杆*/
	  private int zhong_yang_cha_su_qi_suo;				/*中央差速器锁*/
	  private int an_quan_dai_qi_nang;					/*安全带气囊*/
	  private int dian_zi_xian_su;						/*电子限速*/
	  private int kui_suo_shi_zhi_dong;					/*溃缩式制动踏板*/
	  private int test13_zhu;							/*未知数据*/
	  private int mang_dian_qian_ce;					/*盲点检测*/
	  private int fa_dong_ji_zu_li;						/*发动机阻力矩控制系统（EDC/MSR）*/
	  private int wan_dao_zhi_dong_kong_zhi;			/*弯道制动控制系统（CBC）*/
	  private float dian_ji_zui_da_niu_ju;				/*电机最大扭矩*/
	  private float xi_tong_dian_ya;					/*系统电压(v)*/
	  private String dian_ji_type;						/*电机类型*/
	  private float pu_tong_chong_dian_time;			/*普通充电时间*/
	  private float kuai_su_chong_dian_time;			/*快速充电时间*/
	  private float dian_chi_dian_ya;					/*电池电压*/
	  private float dian_chi_rong_liang;				/*电池容量*/
	  private String dian_chi_type;					    /*电池类型*/
	  private float bai_gong_li_hao_dian_liang;		    /*百公里耗电量(kw/100km)*/
	  private float chun_dian_zui_gao_xu_hang_li_cheng; /*纯电最高续航里程(km)*/
	  
	  private int yi_che_wang_id;						/*易车网的id*/
	  private String yi_che_wang_image;					/*易车网的小车图片*/
	  private String yi_che_wang_namespell;				/*易车网车名的 拼音*/
	  private int yi_che_car_id; 						/*易车车款id*/
	  private int car_sale_state;						/*销售状态(1:停销; 2:待销; 3:在销)*/
	  private int yi_che_re_du;							/*易车汽车热度、关注度 */
	  
	  public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public int getCpp_detail_id() {
			return cpp_detail_id;
		}
		public void setCpp_detail_id(int cpp_detail_id) {
			this.cpp_detail_id = cpp_detail_id;
		}
		public int getDang_shu() {
			return dang_shu;
		}
		public void setDang_shu(int dang_shu) {
			this.dang_shu = dang_shu;
		}
		public String getShang_jia_bao_jia() {
			return shang_jia_bao_jia;
		}
		public void setShang_jia_bao_jia(String shang_jia_bao_jia) {
			this.shang_jia_bao_jia = shang_jia_bao_jia;
		}
		public int getJiang_jia_you_hui() {
			return jiang_jia_you_hui;
		}
		public void setJiang_jia_you_hui(int jiang_jia_you_hui) {
			this.jiang_jia_you_hui = jiang_jia_you_hui;
		}
		public String getBao_xiu_zheng_ce() {
			return bao_xiu_zheng_ce;
		}
		public void setBao_xiu_zheng_ce(String bao_xiu_zheng_ce) {
			this.bao_xiu_zheng_ce = bao_xiu_zheng_ce;
		}
		public String getChe_chuan_shui_jian_mian() {
			return che_chuan_shui_jian_mian;
		}
		public void setChe_chuan_shui_jian_mian(String che_chuan_shui_jian_mian) {
			this.che_chuan_shui_jian_mian = che_chuan_shui_jian_mian;
		}
		public float getShi_qu_gong_kuang_you_hao() {
			return shi_qu_gong_kuang_you_hao;
		}
		public void setShi_qu_gong_kuang_you_hao(float shi_qu_gong_kuang_you_hao) {
			this.shi_qu_gong_kuang_you_hao = shi_qu_gong_kuang_you_hao;
		}
		public float getShi_jiao_gong_kuang_you_hao() {
			return shi_jiao_gong_kuang_you_hao;
		}
		public void setShi_jiao_gong_kuang_you_hao(float shi_jiao_gong_kuang_you_hao) {
			this.shi_jiao_gong_kuang_you_hao = shi_jiao_gong_kuang_you_hao;
		}
		public float getZong_he_gong_kuang_you_hao() {
			return zong_he_gong_kuang_you_hao;
		}
		public void setZong_he_gong_kuang_you_hao(float zong_he_gong_kuang_you_hao) {
			this.zong_he_gong_kuang_you_hao = zong_he_gong_kuang_you_hao;
		}
		public float getWang_you_you_hao() {
			return wang_you_you_hao;
		}
		public void setWang_you_you_hao(float wang_you_you_hao) {
			this.wang_you_you_hao = wang_you_you_hao;
		}
		public int getZui_gao_che_su() {
			return zui_gao_che_su;
		}
		public void setZui_gao_che_su(int zui_gao_che_su) {
			this.zui_gao_che_su = zui_gao_che_su;
		}
		public String getChe_shen_yan_se() {
			return che_shen_yan_se;
		}
		public void setChe_shen_yan_se(String che_shen_yan_se) {
			this.che_shen_yan_se = che_shen_yan_se;
		}
		public int getChe_chang() {
			return che_chang;
		}
		public void setChe_chang(int che_chang) {
			this.che_chang = che_chang;
		}
		public int getChe_kuan() {
			return che_kuan;
		}
		public void setChe_kuan(int che_kuan) {
			this.che_kuan = che_kuan;
		}
		public int getChe_gao() {
			return che_gao;
		}
		public void setChe_gao(int che_gao) {
			this.che_gao = che_gao;
		}
		public int getZhou_ju() {
			return zhou_ju;
		}
		public void setZhou_ju(int zhou_ju) {
			this.zhou_ju = zhou_ju;
		}
		public int getQian_lun_ju() {
			return qian_lun_ju;
		}
		public void setQian_lun_ju(int qian_lun_ju) {
			this.qian_lun_ju = qian_lun_ju;
		}
		public int getHou_lun_ju() {
			return hou_lun_ju;
		}
		public void setHou_lun_ju(int hou_lun_ju) {
			this.hou_lun_ju = hou_lun_ju;
		}
		public float getYi_che_shi_ce_you_hao() {
			return yi_che_shi_ce_you_hao;
		}
		public void setYi_che_shi_ce_you_hao(float yi_che_shi_ce_you_hao) {
			this.yi_che_shi_ce_you_hao = yi_che_shi_ce_you_hao;
		}
		public float getYi_che_shi_ce_gong_li_jia_su() {
			return yi_che_shi_ce_gong_li_jia_su;
		}
		public void setYi_che_shi_ce_gong_li_jia_su(float yi_che_shi_ce_gong_li_jia_su) {
			this.yi_che_shi_ce_gong_li_jia_su = yi_che_shi_ce_gong_li_jia_su;
		}
		public int getZheng_bei_zhi_liang() {
			return zheng_bei_zhi_liang;
		}
		public void setZheng_bei_zhi_liang(int zheng_bei_zhi_liang) {
			this.zheng_bei_zhi_liang = zheng_bei_zhi_liang;
		}
		public int getMan_zai_zhi_liang() {
			return man_zai_zhi_liang;
		}
		public void setMan_zai_zhi_liang(int man_zai_zhi_liang) {
			this.man_zai_zhi_liang = man_zai_zhi_liang;
		}
		public float getZui_xiao_li_di_jian_xi() {
			return zui_xiao_li_di_jian_xi;
		}
		public void setZui_xiao_li_di_jian_xi(float zui_xiao_li_di_jian_xi) {
			this.zui_xiao_li_di_jian_xi = zui_xiao_li_di_jian_xi;
		}
		public String getJie_jin_jiao() {
			return jie_jin_jiao;
		}
		public void setJie_jin_jiao(String jie_jin_jiao) {
			this.jie_jin_jiao = jie_jin_jiao;
		}
		public String getTong_guo_jiao() {
			return tong_guo_jiao;
		}
		public void setTong_guo_jiao(String tong_guo_jiao) {
			this.tong_guo_jiao = tong_guo_jiao;
		}
		public String getLi_qu_jiao() {
			return li_qu_jiao;
		}
		public void setLi_qu_jiao(String li_qu_jiao) {
			this.li_qu_jiao = li_qu_jiao;
		}
		public int getXing_li_xiang_rong_ji() {
			return xing_li_xiang_rong_ji;
		}
		public void setXing_li_xiang_rong_ji(int xing_li_xiang_rong_ji) {
			this.xing_li_xiang_rong_ji = xing_li_xiang_rong_ji;
		}
		public int getXing_li_xiang_zui_da_kuo_zhan_rong_ji() {
			return xing_li_xiang_zui_da_kuo_zhan_rong_ji;
		}
		public void setXing_li_xiang_zui_da_kuo_zhan_rong_ji(
				int xing_li_xiang_zui_da_kuo_zhan_rong_ji) {
			this.xing_li_xiang_zui_da_kuo_zhan_rong_ji = xing_li_xiang_zui_da_kuo_zhan_rong_ji;
		}
		public int getXing_li_xiang_gai_kai_he() {
			return xing_li_xiang_gai_kai_he;
		}
		public void setXing_li_xiang_gai_kai_he(int xing_li_xiang_gai_kai_he) {
			this.xing_li_xiang_gai_kai_he = xing_li_xiang_gai_kai_he;
		}
		public String getXing_li_xiang_da_kai_fang_shi() {
			return xing_li_xiang_da_kai_fang_shi;
		}
		public void setXing_li_xiang_da_kai_fang_shi(
				String xing_li_xiang_da_kai_fang_shi) {
			this.xing_li_xiang_da_kai_fang_shi = xing_li_xiang_da_kai_fang_shi;
		}
		public String getChe_ding_xing_shi() {
			return che_ding_xing_shi;
		}
		public void setChe_ding_xing_shi(String che_ding_xing_shi) {
			this.che_ding_xing_shi = che_ding_xing_shi;
		}
		public String getChe_peng_xing_shi() {
			return che_peng_xing_shi;
		}
		public void setChe_peng_xing_shi(String che_peng_xing_shi) {
			this.che_peng_xing_shi = che_peng_xing_shi;
		}
		public int getChe_ding_xing_li_xiang_jia() {
			return che_ding_xing_li_xiang_jia;
		}
		public void setChe_ding_xing_li_xiang_jia(int che_ding_xing_li_xiang_jia) {
			this.che_ding_xing_li_xiang_jia = che_ding_xing_li_xiang_jia;
		}
		public int getHou_dao_liu_wei_yi() {
			return hou_dao_liu_wei_yi;
		}
		public void setHou_dao_liu_wei_yi(int hou_dao_liu_wei_yi) {
			this.hou_dao_liu_wei_yi = hou_dao_liu_wei_yi;
		}
		public int getYun_dong_bao_wei() {
			return yun_dong_bao_wei;
		}
		public void setYun_dong_bao_wei(int yun_dong_bao_wei) {
			this.yun_dong_bao_wei = yun_dong_bao_wei;
		}
		public String getFa_dong_ji_wei_zhi() {
			return fa_dong_ji_wei_zhi;
		}
		public void setFa_dong_ji_wei_zhi(String fa_dong_ji_wei_zhi) {
			this.fa_dong_ji_wei_zhi = fa_dong_ji_wei_zhi;
		}
		public String getFa_dong_ji_xing_hao() {
			return fa_dong_ji_xing_hao;
		}
		public void setFa_dong_ji_xing_hao(String fa_dong_ji_xing_hao) {
			this.fa_dong_ji_xing_hao = fa_dong_ji_xing_hao;
		}
		public int getPai_liang_hao_sheng() {
			return pai_liang_hao_sheng;
		}
		public void setPai_liang_hao_sheng(int pai_liang_hao_sheng) {
			this.pai_liang_hao_sheng = pai_liang_hao_sheng;
		}
		public String getJin_qi_xing_shi() {
			return jin_qi_xing_shi;
		}
		public void setJin_qi_xing_shi(String jin_qi_xing_shi) {
			this.jin_qi_xing_shi = jin_qi_xing_shi;
		}
		public String getQi_gang_pai_lie_xing_shi() {
			return qi_gang_pai_lie_xing_shi;
		}
		public void setQi_gang_pai_lie_xing_shi(String qi_gang_pai_lie_xing_shi) {
			this.qi_gang_pai_lie_xing_shi = qi_gang_pai_lie_xing_shi;
		}
		public int getQi_gang_shu() {
			return qi_gang_shu;
		}
		public void setQi_gang_shu(int qi_gang_shu) {
			this.qi_gang_shu = qi_gang_shu;
		}
		public int getMei_gang_qi_men_shu() {
			return mei_gang_qi_men_shu;
		}
		public void setMei_gang_qi_men_shu(int mei_gang_qi_men_shu) {
			this.mei_gang_qi_men_shu = mei_gang_qi_men_shu;
		}
		public String getQi_men_jie_gou() {
			return qi_men_jie_gou;
		}
		public void setQi_men_jie_gou(String qi_men_jie_gou) {
			this.qi_men_jie_gou = qi_men_jie_gou;
		}
		public String getYa_suo_bi() {
			return ya_suo_bi;
		}
		public void setYa_suo_bi(String ya_suo_bi) {
			this.ya_suo_bi = ya_suo_bi;
		}
		public String getGang_jing() {
			return gang_jing;
		}
		public void setGang_jing(String gang_jing) {
			this.gang_jing = gang_jing;
		}
		public String getXing_cheng() {
			return xing_cheng;
		}
		public void setXing_cheng(String xing_cheng) {
			this.xing_cheng = xing_cheng;
		}
		public float getZui_da_ma_li() {
			return zui_da_ma_li;
		}
		public void setZui_da_ma_li(float zui_da_ma_li) {
			this.zui_da_ma_li = zui_da_ma_li;
		}
		public float getZui_da_gong_lv() {
			return zui_da_gong_lv;
		}
		public void setZui_da_gong_lv(float zui_da_gong_lv) {
			this.zui_da_gong_lv = zui_da_gong_lv;
		}
		public String getZui_da_gong_lv_zhuan_su() {
			return zui_da_gong_lv_zhuan_su;
		}
		public void setZui_da_gong_lv_zhuan_su(String zui_da_gong_lv_zhuan_su) {
			this.zui_da_gong_lv_zhuan_su = zui_da_gong_lv_zhuan_su;
		}
		public float getZui_da_niu_ju() {
			return zui_da_niu_ju;
		}
		public void setZui_da_niu_ju(float zui_da_niu_ju) {
			this.zui_da_niu_ju = zui_da_niu_ju;
		}
		public String getZui_da_niu_ju_zhuan_su() {
			return zui_da_niu_ju_zhuan_su;
		}
		public void setZui_da_niu_ju_zhuan_su(String zui_da_niu_ju_zhuan_su) {
			this.zui_da_niu_ju_zhuan_su = zui_da_niu_ju_zhuan_su;
		}
		public String getTe_you_ji_shu() {
			return te_you_ji_shu;
		}
		public void setTe_you_ji_shu(String te_you_ji_shu) {
			this.te_you_ji_shu = te_you_ji_shu;
		}
		public String getRan_you_biao_hao() {
			return ran_you_biao_hao;
		}
		public void setRan_you_biao_hao(String ran_you_biao_hao) {
			this.ran_you_biao_hao = ran_you_biao_hao;
		}
		public String getGong_you_fang_shi() {
			return gong_you_fang_shi;
		}
		public void setGong_you_fang_shi(String gong_you_fang_shi) {
			this.gong_you_fang_shi = gong_you_fang_shi;
		}
		public float getRan_you_xiang_rong_ji() {
			return ran_you_xiang_rong_ji;
		}
		public void setRan_you_xiang_rong_ji(float ran_you_xiang_rong_ji) {
			this.ran_you_xiang_rong_ji = ran_you_xiang_rong_ji;
		}
		public String getGang_gai_cai_liao() {
			return gang_gai_cai_liao;
		}
		public void setGang_gai_cai_liao(String gang_gai_cai_liao) {
			this.gang_gai_cai_liao = gang_gai_cai_liao;
		}
		public String getGang_ti_cai_liao() {
			return gang_ti_cai_liao;
		}
		public void setGang_ti_cai_liao(String gang_ti_cai_liao) {
			this.gang_ti_cai_liao = gang_ti_cai_liao;
		}
		public int getHuan_dang_bo_pian() {
			return huan_dang_bo_pian;
		}
		public void setHuan_dang_bo_pian(int huan_dang_bo_pian) {
			this.huan_dang_bo_pian = huan_dang_bo_pian;
		}
		public float getDian_dong_zui_da_gong_lv() {
			return dian_dong_zui_da_gong_lv;
		}
		public void setDian_dong_zui_da_gong_lv(float dian_dong_zui_da_gong_lv) {
			this.dian_dong_zui_da_gong_lv = dian_dong_zui_da_gong_lv;
		}
		public float getDian_ji_e_ding_gong_lv() {
			return dian_ji_e_ding_gong_lv;
		}
		public void setDian_ji_e_ding_gong_lv(float dian_ji_e_ding_gong_lv) {
			this.dian_ji_e_ding_gong_lv = dian_ji_e_ding_gong_lv;
		}
		public String getHuan_bao_biao_zhun() {
			return huan_bao_biao_zhun;
		}
		public void setHuan_bao_biao_zhun(String huan_bao_biao_zhun) {
			this.huan_bao_biao_zhun = huan_bao_biao_zhun;
		}
		public int getQi_dong_xi_tong() {
			return qi_dong_xi_tong;
		}
		public void setQi_dong_xi_tong(int qi_dong_xi_tong) {
			this.qi_dong_xi_tong = qi_dong_xi_tong;
		}
		public String getChe_ti_jie_gou() {
			return che_ti_jie_gou;
		}
		public void setChe_ti_jie_gou(String che_ti_jie_gou) {
			this.che_ti_jie_gou = che_ti_jie_gou;
		}
		public float getZui_xiao_zhuan_wan_ban_jing() {
			return zui_xiao_zhuan_wan_ban_jing;
		}
		public void setZui_xiao_zhuan_wan_ban_jing(float zui_xiao_zhuan_wan_ban_jing) {
			this.zui_xiao_zhuan_wan_ban_jing = zui_xiao_zhuan_wan_ban_jing;
		}
		public String getZhuan_xiang_zhu_li() {
			return zhuan_xiang_zhu_li;
		}
		public void setZhuan_xiang_zhu_li(String zhuan_xiang_zhu_li) {
			this.zhuan_xiang_zhu_li = zhuan_xiang_zhu_li;
		}
		public String getQian_zhi_dong_lei_xing() {
			return qian_zhi_dong_lei_xing;
		}
		public void setQian_zhi_dong_lei_xing(String qian_zhi_dong_lei_xing) {
			this.qian_zhi_dong_lei_xing = qian_zhi_dong_lei_xing;
		}
		public String getHou_zhi_dong_lei_xing() {
			return hou_zhi_dong_lei_xing;
		}
		public void setHou_zhi_dong_lei_xing(String hou_zhi_dong_lei_xing) {
			this.hou_zhi_dong_lei_xing = hou_zhi_dong_lei_xing;
		}
		public String getZhu_che_zhi_dong_lei_xing() {
			return zhu_che_zhi_dong_lei_xing;
		}
		public void setZhu_che_zhi_dong_lei_xing(String zhu_che_zhi_dong_lei_xing) {
			this.zhu_che_zhi_dong_lei_xing = zhu_che_zhi_dong_lei_xing;
		}
		public int getKong_qi_xian_gua() {
			return kong_qi_xian_gua;
		}
		public void setKong_qi_xian_gua(int kong_qi_xian_gua) {
			this.kong_qi_xian_gua = kong_qi_xian_gua;
		}
		public int getKe_tiao_xuan_gua() {
			return ke_tiao_xuan_gua;
		}
		public void setKe_tiao_xuan_gua(int ke_tiao_xuan_gua) {
			this.ke_tiao_xuan_gua = ke_tiao_xuan_gua;
		}
		public String getQian_xuan_gua_lei_xing() {
			return qian_xuan_gua_lei_xing;
		}
		public void setQian_xuan_gua_lei_xing(String qian_xuan_gua_lei_xing) {
			this.qian_xuan_gua_lei_xing = qian_xuan_gua_lei_xing;
		}
		public String getHou_xuan_gua_lei_xing() {
			return hou_xuan_gua_lei_xing;
		}
		public void setHou_xuan_gua_lei_xing(String hou_xuan_gua_lei_xing) {
			this.hou_xuan_gua_lei_xing = hou_xuan_gua_lei_xing;
		}
		public int getJia_shi_wei_an_quan_qi_nang() {
			return jia_shi_wei_an_quan_qi_nang;
		}
		public void setJia_shi_wei_an_quan_qi_nang(int jia_shi_wei_an_quan_qi_nang) {
			this.jia_shi_wei_an_quan_qi_nang = jia_shi_wei_an_quan_qi_nang;
		}
		public int getFu_jia_shi_wei_an_quan_qi_nang() {
			return fu_jia_shi_wei_an_quan_qi_nang;
		}
		public void setFu_jia_shi_wei_an_quan_qi_nang(int fu_jia_shi_wei_an_quan_qi_nang) {
			this.fu_jia_shi_wei_an_quan_qi_nang = fu_jia_shi_wei_an_quan_qi_nang;
		}
		public int getQian_pai_ce_an_quan_qi_nang() {
			return qian_pai_ce_an_quan_qi_nang;
		}
		public void setQian_pai_ce_an_quan_qi_nang(int qian_pai_ce_an_quan_qi_nang) {
			this.qian_pai_ce_an_quan_qi_nang = qian_pai_ce_an_quan_qi_nang;
		}
		public int getQian_pai_tou_bu_qi_nang() {
			return qian_pai_tou_bu_qi_nang;
		}
		public void setQian_pai_tou_bu_qi_nang(int qian_pai_tou_bu_qi_nang) {
			this.qian_pai_tou_bu_qi_nang = qian_pai_tou_bu_qi_nang;
		}
		public int getXi_bu_qi_nang() {
			return xi_bu_qi_nang;
		}
		public void setXi_bu_qi_nang(int xi_bu_qi_nang) {
			this.xi_bu_qi_nang = xi_bu_qi_nang;
		}
		public int getHou_pai_ce_an_quan_qi_nang() {
			return hou_pai_ce_an_quan_qi_nang;
		}
		public void setHou_pai_ce_an_quan_qi_nang(int hou_pai_ce_an_quan_qi_nang) {
			this.hou_pai_ce_an_quan_qi_nang = hou_pai_ce_an_quan_qi_nang;
		}
		public int getHou_pai_tou_bu_qi_nang() {
			return hou_pai_tou_bu_qi_nang;
		}
		public void setHou_pai_tou_bu_qi_nang(int hou_pai_tou_bu_qi_nang) {
			this.hou_pai_tou_bu_qi_nang = hou_pai_tou_bu_qi_nang;
		}
		public int getAn_quan_dai_wei_xi_ti_shi() {
			return an_quan_dai_wei_xi_ti_shi;
		}
		public void setAn_quan_dai_wei_xi_ti_shi(int an_quan_dai_wei_xi_ti_shi) {
			this.an_quan_dai_wei_xi_ti_shi = an_quan_dai_wei_xi_ti_shi;
		}
		public int getAn_quan_dai_xian_li_gong_neng() {
			return an_quan_dai_xian_li_gong_neng;
		}
		public void setAn_quan_dai_xian_li_gong_neng(int an_quan_dai_xian_li_gong_neng) {
			this.an_quan_dai_xian_li_gong_neng = an_quan_dai_xian_li_gong_neng;
		}
		public int getAn_quan_dai_yu_shou_jin_gong_neng() {
			return an_quan_dai_yu_shou_jin_gong_neng;
		}
		public void setAn_quan_dai_yu_shou_jin_gong_neng(
				int an_quan_dai_yu_shou_jin_gong_neng) {
			this.an_quan_dai_yu_shou_jin_gong_neng = an_quan_dai_yu_shou_jin_gong_neng;
		}
		public String getQian_an_quan_dai_tiao_jie() {
			return qian_an_quan_dai_tiao_jie;
		}
		public void setQian_an_quan_dai_tiao_jie(String qian_an_quan_dai_tiao_jie) {
			this.qian_an_quan_dai_tiao_jie = qian_an_quan_dai_tiao_jie;
		}
		public int getHou_pai_an_quan_dai() {
			return hou_pai_an_quan_dai;
		}
		public void setHou_pai_an_quan_dai(int hou_pai_an_quan_dai) {
			this.hou_pai_an_quan_dai = hou_pai_an_quan_dai;
		}
		public int getHou_pai_zhong_jian_san_dian() {
			return hou_pai_zhong_jian_san_dian;
		}
		public void setHou_pai_zhong_jian_san_dian(int hou_pai_zhong_jian_san_dian) {
			this.hou_pai_zhong_jian_san_dian = hou_pai_zhong_jian_san_dian;
		}
		public int getTai_ya_jian_ce_zhuang_zhi() {
			return tai_ya_jian_ce_zhuang_zhi;
		}
		public void setTai_ya_jian_ce_zhuang_zhi(int tai_ya_jian_ce_zhuang_zhi) {
			this.tai_ya_jian_ce_zhuang_zhi = tai_ya_jian_ce_zhuang_zhi;
		}
		public int getLing_ya_xu_xing() {
			return ling_ya_xu_xing;
		}
		public void setLing_ya_xu_xing(int ling_ya_xu_xing) {
			this.ling_ya_xu_xing = ling_ya_xu_xing;
		}
		public int getKe_kui_suo_zhuan_xiang_zhu() {
			return ke_kui_suo_zhuan_xiang_zhu;
		}
		public void setKe_kui_suo_zhuan_xiang_zhu(int ke_kui_suo_zhuan_xiang_zhu) {
			this.ke_kui_suo_zhuan_xiang_zhu = ke_kui_suo_zhuan_xiang_zhu;
		}
		public int getChe_nei_zhong_kong_suo() {
			return che_nei_zhong_kong_suo;
		}
		public void setChe_nei_zhong_kong_suo(int che_nei_zhong_kong_suo) {
			this.che_nei_zhong_kong_suo = che_nei_zhong_kong_suo;
		}
		public int getZhong_kong_men_suo() {
			return zhong_kong_men_suo;
		}
		public void setZhong_kong_men_suo(int zhong_kong_men_suo) {
			this.zhong_kong_men_suo = zhong_kong_men_suo;
		}
		public int getEr_tong_suo() {
			return er_tong_suo;
		}
		public void setEr_tong_suo(int er_tong_suo) {
			this.er_tong_suo = er_tong_suo;
		}
		public int getYao_kong_yao_shi() {
			return yao_kong_yao_shi;
		}
		public void setYao_kong_yao_shi(int yao_kong_yao_shi) {
			this.yao_kong_yao_shi = yao_kong_yao_shi;
		}
		public int getWu_yao_shi_qi_dong_xi_tong() {
			return wu_yao_shi_qi_dong_xi_tong;
		}
		public void setWu_yao_shi_qi_dong_xi_tong(int wu_yao_shi_qi_dong_xi_tong) {
			this.wu_yao_shi_qi_dong_xi_tong = wu_yao_shi_qi_dong_xi_tong;
		}
		public int getFa_dong_ji_dian_zi_fang_dao() {
			return fa_dong_ji_dian_zi_fang_dao;
		}
		public void setFa_dong_ji_dian_zi_fang_dao(int fa_dong_ji_dian_zi_fang_dao) {
			this.fa_dong_ji_dian_zi_fang_dao = fa_dong_ji_dian_zi_fang_dao;
		}
		public String getQian_lun_tai_gui_ge() {
			return qian_lun_tai_gui_ge;
		}
		public void setQian_lun_tai_gui_ge(String qian_lun_tai_gui_ge) {
			this.qian_lun_tai_gui_ge = qian_lun_tai_gui_ge;
		}
		public String getHou_lun_tai_gui_ge() {
			return hou_lun_tai_gui_ge;
		}
		public void setHou_lun_tai_gui_ge(String hou_lun_tai_gui_ge) {
			this.hou_lun_tai_gui_ge = hou_lun_tai_gui_ge;
		}
		public String getQian_lun_gu_gui_ge() {
			return qian_lun_gu_gui_ge;
		}
		public void setQian_lun_gu_gui_ge(String qian_lun_gu_gui_ge) {
			this.qian_lun_gu_gui_ge = qian_lun_gu_gui_ge;
		}
		public String getHou_lun_gu_gui_ge() {
			return hou_lun_gu_gui_ge;
		}
		public void setHou_lun_gu_gui_ge(String hou_lun_gu_gui_ge) {
			this.hou_lun_gu_gui_ge = hou_lun_gu_gui_ge;
		}
		public String getBei_tai_lei_xing() {
			return bei_tai_lei_xing;
		}
		public void setBei_tai_lei_xing(String bei_tai_lei_xing) {
			this.bei_tai_lei_xing = bei_tai_lei_xing;
		}
		public String getLun_gu_cai_liao() {
			return lun_gu_cai_liao;
		}
		public void setLun_gu_cai_liao(String lun_gu_cai_liao) {
			this.lun_gu_cai_liao = lun_gu_cai_liao;
		}
		public int getSha_che_fang_bao_si() {
			return sha_che_fang_bao_si;
		}
		public void setSha_che_fang_bao_si(int sha_che_fang_bao_si) {
			this.sha_che_fang_bao_si = sha_che_fang_bao_si;
		}
		public int getDian_zi_zhi_dong_li_fen_pei_xi_tong() {
			return dian_zi_zhi_dong_li_fen_pei_xi_tong;
		}
		public void setDian_zi_zhi_dong_li_fen_pei_xi_tong(
				int dian_zi_zhi_dong_li_fen_pei_xi_tong) {
			this.dian_zi_zhi_dong_li_fen_pei_xi_tong = dian_zi_zhi_dong_li_fen_pei_xi_tong;
		}
		public int getSha_che_fu_zhu() {
			return sha_che_fu_zhu;
		}
		public void setSha_che_fu_zhu(int sha_che_fu_zhu) {
			this.sha_che_fu_zhu = sha_che_fu_zhu;
		}
		public int getQian_yin_li_kong_zhi() {
			return qian_yin_li_kong_zhi;
		}
		public void setQian_yin_li_kong_zhi(int qian_yin_li_kong_zhi) {
			this.qian_yin_li_kong_zhi = qian_yin_li_kong_zhi;
		}
		public int getDong_tai_wen_ding_kong_zhi_xi_tong() {
			return dong_tai_wen_ding_kong_zhi_xi_tong;
		}
		public void setDong_tai_wen_ding_kong_zhi_xi_tong(
				int dong_tai_wen_ding_kong_zhi_xi_tong) {
			this.dong_tai_wen_ding_kong_zhi_xi_tong = dong_tai_wen_ding_kong_zhi_xi_tong;
		}
		public int getSui_su_zhu_li_zhuan_xiang_tiao_jie() {
			return sui_su_zhu_li_zhuan_xiang_tiao_jie;
		}
		public void setSui_su_zhu_li_zhuan_xiang_tiao_jie(
				int sui_su_zhu_li_zhuan_xiang_tiao_jie) {
			this.sui_su_zhu_li_zhuan_xiang_tiao_jie = sui_su_zhu_li_zhuan_xiang_tiao_jie;
		}
		public int getZi_dong_zhu_che() {
			return zi_dong_zhu_che;
		}
		public void setZi_dong_zhu_che(int zi_dong_zhu_che) {
			this.zi_dong_zhu_che = zi_dong_zhu_che;
		}
		public int getShang_po_fu_zhu() {
			return shang_po_fu_zhu;
		}
		public void setShang_po_fu_zhu(int shang_po_fu_zhu) {
			this.shang_po_fu_zhu = shang_po_fu_zhu;
		}
		public int getDou_po_huan_jiang() {
			return dou_po_huan_jiang;
		}
		public void setDou_po_huan_jiang(int dou_po_huan_jiang) {
			this.dou_po_huan_jiang = dou_po_huan_jiang;
		}
		public int getBo_che_lei_da() {
			return bo_che_lei_da;
		}
		public void setBo_che_lei_da(int bo_che_lei_da) {
			this.bo_che_lei_da = bo_che_lei_da;
		}
		public int getDao_che_lei_da() {
			return dao_che_lei_da;
		}
		public void setDao_che_lei_da(int dao_che_lei_da) {
			this.dao_che_lei_da = dao_che_lei_da;
		}
		public int getDao_che_ying_xiang() {
			return dao_che_ying_xiang;
		}
		public void setDao_che_ying_xiang(int dao_che_ying_xiang) {
			this.dao_che_ying_xiang = dao_che_ying_xiang;
		}
		public int getQuan_jing_she_xiang_tou() {
			return quan_jing_she_xiang_tou;
		}
		public void setQuan_jing_she_xiang_tou(int quan_jing_she_xiang_tou) {
			this.quan_jing_she_xiang_tou = quan_jing_she_xiang_tou;
		}
		public int getDing_su_xun_hang() {
			return ding_su_xun_hang;
		}
		public void setDing_su_xun_hang(int ding_su_xun_hang) {
			this.ding_su_xun_hang = ding_su_xun_hang;
		}
		public int getZi_shi_ying_xun_hang() {
			return zi_shi_ying_xun_hang;
		}
		public void setZi_shi_ying_xun_hang(int zi_shi_ying_xun_hang) {
			this.zi_shi_ying_xun_hang = zi_shi_ying_xun_hang;
		}
		public int getGps_dao_hang() {
			return gps_dao_hang;
		}
		public void setGps_dao_hang(int gps_dao_hang) {
			this.gps_dao_hang = gps_dao_hang;
		}
		public int getRen_ji_jiao_hu_xi_tong() {
			return ren_ji_jiao_hu_xi_tong;
		}
		public void setRen_ji_jiao_hu_xi_tong(int ren_ji_jiao_hu_xi_tong) {
			this.ren_ji_jiao_hu_xi_tong = ren_ji_jiao_hu_xi_tong;
		}
		public int getZi_dong_bo_che_ru_wei() {
			return zi_dong_bo_che_ru_wei;
		}
		public void setZi_dong_bo_che_ru_wei(int zi_dong_bo_che_ru_wei) {
			this.zi_dong_bo_che_ru_wei = zi_dong_bo_che_ru_wei;
		}
		public int getBing_xian_fu_zhu() {
			return bing_xian_fu_zhu;
		}
		public void setBing_xian_fu_zhu(int bing_xian_fu_zhu) {
			this.bing_xian_fu_zhu = bing_xian_fu_zhu;
		}
		public int getZhu_dong_sha_che() {
			return zhu_dong_sha_che;
		}
		public void setZhu_dong_sha_che(int zhu_dong_sha_che) {
			this.zhu_dong_sha_che = zhu_dong_sha_che;
		}
		public int getZheng_ti_zhu_dong_zhuan_xiang_xi_tong() {
			return zheng_ti_zhu_dong_zhuan_xiang_xi_tong;
		}
		public void setZheng_ti_zhu_dong_zhuan_xiang_xi_tong(
				int zheng_ti_zhu_dong_zhuan_xiang_xi_tong) {
			this.zheng_ti_zhu_dong_zhuan_xiang_xi_tong = zheng_ti_zhu_dong_zhuan_xiang_xi_tong;
		}
		public int getYe_shi_xi_tong() {
			return ye_shi_xi_tong;
		}
		public void setYe_shi_xi_tong(int ye_shi_xi_tong) {
			this.ye_shi_xi_tong = ye_shi_xi_tong;
		}
		public String getKai_men_fang_shi() {
			return kai_men_fang_shi;
		}
		public void setKai_men_fang_shi(String kai_men_fang_shi) {
			this.kai_men_fang_shi = kai_men_fang_shi;
		}
		public String getDian_dong_che_chuang() {
			return dian_dong_che_chuang;
		}
		public void setDian_dong_che_chuang(String dian_dong_che_chuang) {
			this.dian_dong_che_chuang = dian_dong_che_chuang;
		}
		public String getFang_zi_wai_xian() {
			return fang_zi_wai_xian;
		}
		public void setFang_zi_wai_xian(String fang_zi_wai_xian) {
			this.fang_zi_wai_xian = fang_zi_wai_xian;
		}
		public String getDian_dong_chuang_fang_jia() {
			return dian_dong_chuang_fang_jia;
		}
		public void setDian_dong_chuang_fang_jia(String dian_dong_chuang_fang_jia) {
			this.dian_dong_chuang_fang_jia = dian_dong_chuang_fang_jia;
		}
		public String getTian_chuang_kai_he_fang_shi() {
			return tian_chuang_kai_he_fang_shi;
		}
		public void setTian_chuang_kai_he_fang_shi(String tian_chuang_kai_he_fang_shi) {
			this.tian_chuang_kai_he_fang_shi = tian_chuang_kai_he_fang_shi;
		}
		public String getTian_chuang_xing_shi() {
			return tian_chuang_xing_shi;
		}
		public void setTian_chuang_xing_shi(String tian_chuang_xing_shi) {
			this.tian_chuang_xing_shi = tian_chuang_xing_shi;
		}
		public int getHou_chuang_zhe_yang_lian() {
			return hou_chuang_zhe_yang_lian;
		}
		public void setHou_chuang_zhe_yang_lian(int hou_chuang_zhe_yang_lian) {
			this.hou_chuang_zhe_yang_lian = hou_chuang_zhe_yang_lian;
		}
		public int getHou_pai_ce_zhe_yang_lian() {
			return hou_pai_ce_zhe_yang_lian;
		}
		public void setHou_pai_ce_zhe_yang_lian(int hou_pai_ce_zhe_yang_lian) {
			this.hou_pai_ce_zhe_yang_lian = hou_pai_ce_zhe_yang_lian;
		}
		public int getHou_yu_shua_qi() {
			return hou_yu_shua_qi;
		}
		public void setHou_yu_shua_qi(int hou_yu_shua_qi) {
			this.hou_yu_shua_qi = hou_yu_shua_qi;
		}
		public int getGan_ying_yu_shua() {
			return gan_ying_yu_shua;
		}
		public void setGan_ying_yu_shua(int gan_ying_yu_shua) {
			this.gan_ying_yu_shua = gan_ying_yu_shua;
		}
		public int getDian_dong_xi_he_men() {
			return dian_dong_xi_he_men;
		}
		public void setDian_dong_xi_he_men(int dian_dong_xi_he_men) {
			this.dian_dong_xi_he_men = dian_dong_xi_he_men;
		}
		public int getXing_li_xiang_dian_dong_xi_he_men() {
			return xing_li_xiang_dian_dong_xi_he_men;
		}
		public void setXing_li_xiang_dian_dong_xi_he_men(
				int xing_li_xiang_dian_dong_xi_he_men) {
			this.xing_li_xiang_dian_dong_xi_he_men = xing_li_xiang_dian_dong_xi_he_men;
		}
		public int getHou_shi_jing_dai_ce_zhuan_xiang_deng() {
			return hou_shi_jing_dai_ce_zhuan_xiang_deng;
		}
		public void setHou_shi_jing_dai_ce_zhuan_xiang_deng(
				int hou_shi_jing_dai_ce_zhuan_xiang_deng) {
			this.hou_shi_jing_dai_ce_zhuan_xiang_deng = hou_shi_jing_dai_ce_zhuan_xiang_deng;
		}
		public int getWai_hou_shi_jing_ji_yi_gong_neng() {
			return wai_hou_shi_jing_ji_yi_gong_neng;
		}
		public void setWai_hou_shi_jing_ji_yi_gong_neng(
				int wai_hou_shi_jing_ji_yi_gong_neng) {
			this.wai_hou_shi_jing_ji_yi_gong_neng = wai_hou_shi_jing_ji_yi_gong_neng;
		}
		public int getWai_hou_shi_jing_jia_re_gong_neng() {
			return wai_hou_shi_jing_jia_re_gong_neng;
		}
		public void setWai_hou_shi_jing_jia_re_gong_neng(
				int wai_hou_shi_jing_jia_re_gong_neng) {
			this.wai_hou_shi_jing_jia_re_gong_neng = wai_hou_shi_jing_jia_re_gong_neng;
		}
		public int getWai_hou_shi_jing_dian_dong_zhe_die_gong_neng() {
			return wai_hou_shi_jing_dian_dong_zhe_die_gong_neng;
		}
		public void setWai_hou_shi_jing_dian_dong_zhe_die_gong_neng(
				int wai_hou_shi_jing_dian_dong_zhe_die_gong_neng) {
			this.wai_hou_shi_jing_dian_dong_zhe_die_gong_neng = wai_hou_shi_jing_dian_dong_zhe_die_gong_neng;
		}
		public int getWai_hou_shi_jing_dian_dong_tiao_jie() {
			return wai_hou_shi_jing_dian_dong_tiao_jie;
		}
		public void setWai_hou_shi_jing_dian_dong_tiao_jie(
				int wai_hou_shi_jing_dian_dong_tiao_jie) {
			this.wai_hou_shi_jing_dian_dong_tiao_jie = wai_hou_shi_jing_dian_dong_tiao_jie;
		}
		public int getNei_hou_shi_jing_fang_xuan_mu() {
			return nei_hou_shi_jing_fang_xuan_mu;
		}
		public void setNei_hou_shi_jing_fang_xuan_mu(int nei_hou_shi_jing_fang_xuan_mu) {
			this.nei_hou_shi_jing_fang_xuan_mu = nei_hou_shi_jing_fang_xuan_mu;
		}
		public int getZhe_yang_ban_hua_zhuang_jing() {
			return zhe_yang_ban_hua_zhuang_jing;
		}
		public void setZhe_yang_ban_hua_zhuang_jing(int zhe_yang_ban_hua_zhuang_jing) {
			this.zhe_yang_ban_hua_zhuang_jing = zhe_yang_ban_hua_zhuang_jing;
		}
		public String getQian_zhao_deng_lei_xing() {
			return qian_zhao_deng_lei_xing;
		}
		public void setQian_zhao_deng_lei_xing(String qian_zhao_deng_lei_xing) {
			this.qian_zhao_deng_lei_xing = qian_zhao_deng_lei_xing;
		}
		public int getQian_da_deng_zi_dong_kai_bi() {
			return qian_da_deng_zi_dong_kai_bi;
		}
		public void setQian_da_deng_zi_dong_kai_bi(int qian_da_deng_zi_dong_kai_bi) {
			this.qian_da_deng_zi_dong_kai_bi = qian_da_deng_zi_dong_kai_bi;
		}
		public int getQian_zhao_deng_zi_dong_qing_xi() {
			return qian_zhao_deng_zi_dong_qing_xi;
		}
		public void setQian_zhao_deng_zi_dong_qing_xi(int qian_zhao_deng_zi_dong_qing_xi) {
			this.qian_zhao_deng_zi_dong_qing_xi = qian_zhao_deng_zi_dong_qing_xi;
		}
		public int getQian_da_deng_yan_shi_guan_bi() {
			return qian_da_deng_yan_shi_guan_bi;
		}
		public void setQian_da_deng_yan_shi_guan_bi(int qian_da_deng_yan_shi_guan_bi) {
			this.qian_da_deng_yan_shi_guan_bi = qian_da_deng_yan_shi_guan_bi;
		}
		public int getQian_da_deng_sui_dong_zhuan_xiang() {
			return qian_da_deng_sui_dong_zhuan_xiang;
		}
		public void setQian_da_deng_sui_dong_zhuan_xiang(
				int qian_da_deng_sui_dong_zhuan_xiang) {
			this.qian_da_deng_sui_dong_zhuan_xiang = qian_da_deng_sui_dong_zhuan_xiang;
		}
		public int getQian_zhao_deng_zhao_she_fan_wei_tiao_zheng() {
			return qian_zhao_deng_zhao_she_fan_wei_tiao_zheng;
		}
		public void setQian_zhao_deng_zhao_she_fan_wei_tiao_zheng(
				int qian_zhao_deng_zhao_she_fan_wei_tiao_zheng) {
			this.qian_zhao_deng_zhao_she_fan_wei_tiao_zheng = qian_zhao_deng_zhao_she_fan_wei_tiao_zheng;
		}
		public int getHui_che_qian_deng_fang_xuan_mu_gong_neng() {
			return hui_che_qian_deng_fang_xuan_mu_gong_neng;
		}
		public void setHui_che_qian_deng_fang_xuan_mu_gong_neng(
				int hui_che_qian_deng_fang_xuan_mu_gong_neng) {
			this.hui_che_qian_deng_fang_xuan_mu_gong_neng = hui_che_qian_deng_fang_xuan_mu_gong_neng;
		}
		public int getQian_wu_deng() {
			return qian_wu_deng;
		}
		public void setQian_wu_deng(int qian_wu_deng) {
			this.qian_wu_deng = qian_wu_deng;
		}
		public int getChe_xiang_qian_yue_du_deng() {
			return che_xiang_qian_yue_du_deng;
		}
		public void setChe_xiang_qian_yue_du_deng(int che_xiang_qian_yue_du_deng) {
			this.che_xiang_qian_yue_du_deng = che_xiang_qian_yue_du_deng;
		}
		public int getChe_xiang_hou_yue_du_deng() {
			return che_xiang_hou_yue_du_deng;
		}
		public void setChe_xiang_hou_yue_du_deng(int che_xiang_hou_yue_du_deng) {
			this.che_xiang_hou_yue_du_deng = che_xiang_hou_yue_du_deng;
		}
		public int getChe_nei_fen_wei_deng() {
			return che_nei_fen_wei_deng;
		}
		public void setChe_nei_fen_wei_deng(int che_nei_fen_wei_deng) {
			this.che_nei_fen_wei_deng = che_nei_fen_wei_deng;
		}
		public int getRi_jian_xing_che_deng() {
			return ri_jian_xing_che_deng;
		}
		public void setRi_jian_xing_che_deng(int ri_jian_xing_che_deng) {
			this.ri_jian_xing_che_deng = ri_jian_xing_che_deng;
		}
		public int getLed_wei_deng() {
			return led_wei_deng;
		}
		public void setLed_wei_deng(int led_wei_deng) {
			this.led_wei_deng = led_wei_deng;
		}
		public int getGao_wei_zhi_dong_deng() {
			return gao_wei_zhi_dong_deng;
		}
		public void setGao_wei_zhi_dong_deng(int gao_wei_zhi_dong_deng) {
			this.gao_wei_zhi_dong_deng = gao_wei_zhi_dong_deng;
		}
		public int getZhuan_xiang_tou_deng() {
			return zhuan_xiang_tou_deng;
		}
		public void setZhuan_xiang_tou_deng(int zhuan_xiang_tou_deng) {
			this.zhuan_xiang_tou_deng = zhuan_xiang_tou_deng;
		}
		public String getCe_zhuan_xiang_deng() {
			return ce_zhuan_xiang_deng;
		}
		public void setCe_zhuan_xiang_deng(String ce_zhuan_xiang_deng) {
			this.ce_zhuan_xiang_deng = ce_zhuan_xiang_deng;
		}
		public String getXing_li_xiang_deng() {
			return xing_li_xiang_deng;
		}
		public void setXing_li_xiang_deng(String xing_li_xiang_deng) {
			this.xing_li_xiang_deng = xing_li_xiang_deng;
		}
		public int getFang_xiang_pan_qian_hou_tiao_jie() {
			return fang_xiang_pan_qian_hou_tiao_jie;
		}
		public void setFang_xiang_pan_qian_hou_tiao_jie(
				int fang_xiang_pan_qian_hou_tiao_jie) {
			this.fang_xiang_pan_qian_hou_tiao_jie = fang_xiang_pan_qian_hou_tiao_jie;
		}
		public int getFang_xiang_pan_shang_xia_tiao_jie() {
			return fang_xiang_pan_shang_xia_tiao_jie;
		}
		public void setFang_xiang_pan_shang_xia_tiao_jie(
				int fang_xiang_pan_shang_xia_tiao_jie) {
			this.fang_xiang_pan_shang_xia_tiao_jie = fang_xiang_pan_shang_xia_tiao_jie;
		}
		public String getFang_xiang_pan_tiao_jie_fang_shi() {
			return fang_xiang_pan_tiao_jie_fang_shi;
		}
		public void setFang_xiang_pan_tiao_jie_fang_shi(
				String fang_xiang_pan_tiao_jie_fang_shi) {
			this.fang_xiang_pan_tiao_jie_fang_shi = fang_xiang_pan_tiao_jie_fang_shi;
		}
		public int getFang_xiang_pan_ji_yi_she_zhi() {
			return fang_xiang_pan_ji_yi_she_zhi;
		}
		public void setFang_xiang_pan_ji_yi_she_zhi(int fang_xiang_pan_ji_yi_she_zhi) {
			this.fang_xiang_pan_ji_yi_she_zhi = fang_xiang_pan_ji_yi_she_zhi;
		}
		public String getFang_xiang_pan_biao_mian_cai_liao() {
			return fang_xiang_pan_biao_mian_cai_liao;
		}
		public void setFang_xiang_pan_biao_mian_cai_liao(
				String fang_xiang_pan_biao_mian_cai_liao) {
			this.fang_xiang_pan_biao_mian_cai_liao = fang_xiang_pan_biao_mian_cai_liao;
		}
		public int getDuo_gong_neng_fang_xiang_pan() {
			return duo_gong_neng_fang_xiang_pan;
		}
		public void setDuo_gong_neng_fang_xiang_pan(int duo_gong_neng_fang_xiang_pan) {
			this.duo_gong_neng_fang_xiang_pan = duo_gong_neng_fang_xiang_pan;
		}
		public String getDuo_gong_neng_fang_xiang_pan_gong_neng() {
			return duo_gong_neng_fang_xiang_pan_gong_neng;
		}
		public void setDuo_gong_neng_fang_xiang_pan_gong_neng(
				String duo_gong_neng_fang_xiang_pan_gong_neng) {
			this.duo_gong_neng_fang_xiang_pan_gong_neng = duo_gong_neng_fang_xiang_pan_gong_neng;
		}
		public int getXing_che_dian_nao_xian_shi_ping() {
			return xing_che_dian_nao_xian_shi_ping;
		}
		public void setXing_che_dian_nao_xian_shi_ping(
				int xing_che_dian_nao_xian_shi_ping) {
			this.xing_che_dian_nao_xian_shi_ping = xing_che_dian_nao_xian_shi_ping;
		}
		public String getChe_nei_dian_yuan_dian_ya() {
			return che_nei_dian_yuan_dian_ya;
		}
		public void setChe_nei_dian_yuan_dian_ya(String che_nei_dian_yuan_dian_ya) {
			this.che_nei_dian_yuan_dian_ya = che_nei_dian_yuan_dian_ya;
		}
		public int getHud_tai_tou_shu_zi_xian_shi() {
			return hud_tai_tou_shu_zi_xian_shi;
		}
		public void setHud_tai_tou_shu_zi_xian_shi(int hud_tai_tou_shu_zi_xian_shi) {
			this.hud_tai_tou_shu_zi_xian_shi = hud_tai_tou_shu_zi_xian_shi;
		}
		public String getNei_shi_yan_se() {
			return nei_shi_yan_se;
		}
		public void setNei_shi_yan_se(String nei_shi_yan_se) {
			this.nei_shi_yan_se = nei_shi_yan_se;
		}
		public int getHou_pai_bei_jia() {
			return hou_pai_bei_jia;
		}
		public void setHou_pai_bei_jia(int hou_pai_bei_jia) {
			this.hou_pai_bei_jia = hou_pai_bei_jia;
		}
		public int getYun_dong_zuo_yi() {
			return yun_dong_zuo_yi;
		}
		public void setYun_dong_zuo_yi(int yun_dong_zuo_yi) {
			this.yun_dong_zuo_yi = yun_dong_zuo_yi;
		}
		public String getZuo_yi_cai_liao() {
			return zuo_yi_cai_liao;
		}
		public void setZuo_yi_cai_liao(String zuo_yi_cai_liao) {
			this.zuo_yi_cai_liao = zuo_yi_cai_liao;
		}
		public String getJia_shi_zuo_zuo_yi_tiao_jie_fang_shi() {
			return jia_shi_zuo_zuo_yi_tiao_jie_fang_shi;
		}
		public void setJia_shi_zuo_zuo_yi_tiao_jie_fang_shi(
				String jia_shi_zuo_zuo_yi_tiao_jie_fang_shi) {
			this.jia_shi_zuo_zuo_yi_tiao_jie_fang_shi = jia_shi_zuo_zuo_yi_tiao_jie_fang_shi;
		}
		public String getJia_shi_zuo_zuo_yi_tiao_jie_fang_xiang() {
			return jia_shi_zuo_zuo_yi_tiao_jie_fang_xiang;
		}
		public void setJia_shi_zuo_zuo_yi_tiao_jie_fang_xiang(
				String jia_shi_zuo_zuo_yi_tiao_jie_fang_xiang) {
			this.jia_shi_zuo_zuo_yi_tiao_jie_fang_xiang = jia_shi_zuo_zuo_yi_tiao_jie_fang_xiang;
		}
		public String getFu_jia_shi_zuo_yi_tiao_jie_fang_shi() {
			return fu_jia_shi_zuo_yi_tiao_jie_fang_shi;
		}
		public void setFu_jia_shi_zuo_yi_tiao_jie_fang_shi(
				String fu_jia_shi_zuo_yi_tiao_jie_fang_shi) {
			this.fu_jia_shi_zuo_yi_tiao_jie_fang_shi = fu_jia_shi_zuo_yi_tiao_jie_fang_shi;
		}
		public String getFu_jia_shi_zuo_yi_tiao_jie_fang_xiang() {
			return fu_jia_shi_zuo_yi_tiao_jie_fang_xiang;
		}
		public void setFu_jia_shi_zuo_yi_tiao_jie_fang_xiang(
				String fu_jia_shi_zuo_yi_tiao_jie_fang_xiang) {
			this.fu_jia_shi_zuo_yi_tiao_jie_fang_xiang = fu_jia_shi_zuo_yi_tiao_jie_fang_xiang;
		}
		public String getJia_shi_zuo_yao_bu_zhi_cheng_tiao_jie() {
			return jia_shi_zuo_yao_bu_zhi_cheng_tiao_jie;
		}
		public void setJia_shi_zuo_yao_bu_zhi_cheng_tiao_jie(
				String jia_shi_zuo_yao_bu_zhi_cheng_tiao_jie) {
			this.jia_shi_zuo_yao_bu_zhi_cheng_tiao_jie = jia_shi_zuo_yao_bu_zhi_cheng_tiao_jie;
		}
		public String getJia_shi_zuo_jian_bu_zhi_cheng_tiao_jie() {
			return jia_shi_zuo_jian_bu_zhi_cheng_tiao_jie;
		}
		public void setJia_shi_zuo_jian_bu_zhi_cheng_tiao_jie(
				String jia_shi_zuo_jian_bu_zhi_cheng_tiao_jie) {
			this.jia_shi_zuo_jian_bu_zhi_cheng_tiao_jie = jia_shi_zuo_jian_bu_zhi_cheng_tiao_jie;
		}
		public String getQian_zuo_yi_tou_zhen_tiao_jie() {
			return qian_zuo_yi_tou_zhen_tiao_jie;
		}
		public void setQian_zuo_yi_tou_zhen_tiao_jie(
				String qian_zuo_yi_tou_zhen_tiao_jie) {
			this.qian_zuo_yi_tou_zhen_tiao_jie = qian_zuo_yi_tou_zhen_tiao_jie;
		}
		public String getHou_pai_zuo_yi_tiao_jie_fang_shi() {
			return hou_pai_zuo_yi_tiao_jie_fang_shi;
		}
		public void setHou_pai_zuo_yi_tiao_jie_fang_shi(
				String hou_pai_zuo_yi_tiao_jie_fang_shi) {
			this.hou_pai_zuo_yi_tiao_jie_fang_shi = hou_pai_zuo_yi_tiao_jie_fang_shi;
		}
		public String getHou_pai_zuo_wei_fang_dao_bi_li() {
			return hou_pai_zuo_wei_fang_dao_bi_li;
		}
		public void setHou_pai_zuo_wei_fang_dao_bi_li(
				String hou_pai_zuo_wei_fang_dao_bi_li) {
			this.hou_pai_zuo_wei_fang_dao_bi_li = hou_pai_zuo_wei_fang_dao_bi_li;
		}
		public String getQian_zuo_zhong_yang_fu_shou() {
			return qian_zuo_zhong_yang_fu_shou;
		}
		public void setQian_zuo_zhong_yang_fu_shou(String qian_zuo_zhong_yang_fu_shou) {
			this.qian_zuo_zhong_yang_fu_shou = qian_zuo_zhong_yang_fu_shou;
		}
		public String getHou_zuo_zhong_yang_fu_shou() {
			return hou_zuo_zhong_yang_fu_shou;
		}
		public void setHou_zuo_zhong_yang_fu_shou(String hou_zuo_zhong_yang_fu_shou) {
			this.hou_zuo_zhong_yang_fu_shou = hou_zuo_zhong_yang_fu_shou;
		}
		public String getZuo_yi_tong_feng() {
			return zuo_yi_tong_feng;
		}
		public void setZuo_yi_tong_feng(String zuo_yi_tong_feng) {
			this.zuo_yi_tong_feng = zuo_yi_tong_feng;
		}
		public String getZuo_yi_jia_re() {
			return zuo_yi_jia_re;
		}
		public void setZuo_yi_jia_re(String zuo_yi_jia_re) {
			this.zuo_yi_jia_re = zuo_yi_jia_re;
		}
		public String getZuo_yi_an_mo_gong_neng() {
			return zuo_yi_an_mo_gong_neng;
		}
		public void setZuo_yi_an_mo_gong_neng(String zuo_yi_an_mo_gong_neng) {
			this.zuo_yi_an_mo_gong_neng = zuo_yi_an_mo_gong_neng;
		}
		public String getDian_dong_zuo_yi_ji_yi() {
			return dian_dong_zuo_yi_ji_yi;
		}
		public void setDian_dong_zuo_yi_ji_yi(String dian_dong_zuo_yi_ji_yi) {
			this.dian_dong_zuo_yi_ji_yi = dian_dong_zuo_yi_ji_yi;
		}
		public String getEr_tong_an_quan_zuo_yi() {
			return er_tong_an_quan_zuo_yi;
		}
		public void setEr_tong_an_quan_zuo_yi(String er_tong_an_quan_zuo_yi) {
			this.er_tong_an_quan_zuo_yi = er_tong_an_quan_zuo_yi;
		}
		public String getDi_san_pai_zuo_yi() {
			return di_san_pai_zuo_yi;
		}
		public void setDi_san_pai_zuo_yi(String di_san_pai_zuo_yi) {
			this.di_san_pai_zuo_yi = di_san_pai_zuo_yi;
		}
		public String getChe_zai_dian_hua() {
			return che_zai_dian_hua;
		}
		public void setChe_zai_dian_hua(String che_zai_dian_hua) {
			this.che_zai_dian_hua = che_zai_dian_hua;
		}
		public String getLan_ya_xi_tong() {
			return lan_ya_xi_tong;
		}
		public void setLan_ya_xi_tong(String lan_ya_xi_tong) {
			this.lan_ya_xi_tong = lan_ya_xi_tong;
		}
		public String getWai_jie_yin_yuan_jie_kou() {
			return wai_jie_yin_yuan_jie_kou;
		}
		public void setWai_jie_yin_yuan_jie_kou(String wai_jie_yin_yuan_jie_kou) {
			this.wai_jie_yin_yuan_jie_kou = wai_jie_yin_yuan_jie_kou;
		}
		public String getTest_nei_zhi_ying_pan() {
			return test_nei_zhi_ying_pan;
		}
		public void setTest_nei_zhi_ying_pan(String test_nei_zhi_ying_pan) {
			this.test_nei_zhi_ying_pan = test_nei_zhi_ying_pan;
		}
		public String getNei_zhi_ying_pan() {
			return nei_zhi_ying_pan;
		}
		public void setNei_zhi_ying_pan(String nei_zhi_ying_pan) {
			this.nei_zhi_ying_pan = nei_zhi_ying_pan;
		}
		public String getChe_zai_dian_shi() {
			return che_zai_dian_shi;
		}
		public void setChe_zai_dian_shi(String che_zai_dian_shi) {
			this.che_zai_dian_shi = che_zai_dian_shi;
		}
		public int getYang_sheng_qi_shu_liang() {
			return yang_sheng_qi_shu_liang;
		}
		public void setYang_sheng_qi_shu_liang(int yang_sheng_qi_shu_liang) {
			this.yang_sheng_qi_shu_liang = yang_sheng_qi_shu_liang;
		}
		public String getDVD() {
			return DVD;
		}
		public void setDVD(String dVD) {
			DVD = dVD;
		}
		public String getTest_DVD() {
			return test_DVD;
		}
		public void setTest_DVD(String test_DVD) {
			this.test_DVD = test_DVD;
		}
		public String getTest_DVD1() {
			return test_DVD1;
		}
		public void setTest_DVD1(String test_DVD1) {
			this.test_DVD1 = test_DVD1;
		}
		public String getCD_stauts() {
			return CD_stauts;
		}
		public void setCD_stauts(String cD_stauts) {
			CD_stauts = cD_stauts;
		}
		public String getCD_num() {
			return CD_num;
		}
		public void setCD_num(String cD_num) {
			CD_num = cD_num;
		}
		public String getZhong_kong_tai_ye_jing_ping() {
			return zhong_kong_tai_ye_jing_ping;
		}
		public void setZhong_kong_tai_ye_jing_ping(String zhong_kong_tai_ye_jing_ping) {
			this.zhong_kong_tai_ye_jing_ping = zhong_kong_tai_ye_jing_ping;
		}
		public String getHou_pai_ye_jing_ping() {
			return hou_pai_ye_jing_ping;
		}
		public void setHou_pai_ye_jing_ping(String hou_pai_ye_jing_ping) {
			this.hou_pai_ye_jing_ping = hou_pai_ye_jing_ping;
		}
		public String getJia_su_shi_jian() {
			return jia_su_shi_jian;
		}
		public void setJia_su_shi_jian(String jia_su_shi_jian) {
			this.jia_su_shi_jian = jia_su_shi_jian;
		}
		public String getZhi_dong_ju_li() {
			return zhi_dong_ju_li;
		}
		public void setZhi_dong_ju_li(String zhi_dong_ju_li) {
			this.zhi_dong_ju_li = zhi_dong_ju_li;
		}
		public String getYou_hao() {
			return you_hao;
		}
		public void setYou_hao(String you_hao) {
			this.you_hao = you_hao;
		}
		public String getRao_zhuang_su_du() {
			return rao_zhuang_su_du;
		}
		public void setRao_zhuang_su_du(String rao_zhuang_su_du) {
			this.rao_zhuang_su_du = rao_zhuang_su_du;
		}
		public String getChe_nei_dai_su_zao_yin() {
			return che_nei_dai_su_zao_yin;
		}
		public void setChe_nei_dai_su_zao_yin(String che_nei_dai_su_zao_yin) {
			this.che_nei_dai_su_zao_yin = che_nei_dai_su_zao_yin;
		}
		public String getChe_nei_deng_su_zao_yin40() {
			return che_nei_deng_su_zao_yin40;
		}
		public void setChe_nei_deng_su_zao_yin40(String che_nei_deng_su_zao_yin40) {
			this.che_nei_deng_su_zao_yin40 = che_nei_deng_su_zao_yin40;
		}
		public String getChe_nei_deng_su_zao_yin60() {
			return che_nei_deng_su_zao_yin60;
		}
		public void setChe_nei_deng_su_zao_yin60(String che_nei_deng_su_zao_yin60) {
			this.che_nei_deng_su_zao_yin60 = che_nei_deng_su_zao_yin60;
		}
		public String getChe_nei_deng_su_zao_yin80() {
			return che_nei_deng_su_zao_yin80;
		}
		public void setChe_nei_deng_su_zao_yin80(String che_nei_deng_su_zao_yin80) {
			this.che_nei_deng_su_zao_yin80 = che_nei_deng_su_zao_yin80;
		}
		public String getChe_nei_deng_su_zao_yin100() {
			return che_nei_deng_su_zao_yin100;
		}
		public void setChe_nei_deng_su_zao_yin100(String che_nei_deng_su_zao_yin100) {
			this.che_nei_deng_su_zao_yin100 = che_nei_deng_su_zao_yin100;
		}
		public String getChe_nei_deng_su_zao_yin120() {
			return che_nei_deng_su_zao_yin120;
		}
		public void setChe_nei_deng_su_zao_yin120(String che_nei_deng_su_zao_yin120) {
			this.che_nei_deng_su_zao_yin120 = che_nei_deng_su_zao_yin120;
		}
		public String getKong_tiao_kong_zhi_fang_shi() {
			return kong_tiao_kong_zhi_fang_shi;
		}
		public void setKong_tiao_kong_zhi_fang_shi(String kong_tiao_kong_zhi_fang_shi) {
			this.kong_tiao_kong_zhi_fang_shi = kong_tiao_kong_zhi_fang_shi;
		}
		public String getWen_du_fen_qu_kong_zhi() {
			return wen_du_fen_qu_kong_zhi;
		}
		public void setWen_du_fen_qu_kong_zhi(String wen_du_fen_qu_kong_zhi) {
			this.wen_du_fen_qu_kong_zhi = wen_du_fen_qu_kong_zhi;
		}
		public int getWen_du_fen_qu_kong_zhi_num() {
			return wen_du_fen_qu_kong_zhi_num;
		}
		public void setWen_du_fen_qu_kong_zhi_num(int wen_du_fen_qu_kong_zhi_num) {
			this.wen_du_fen_qu_kong_zhi_num = wen_du_fen_qu_kong_zhi_num;
		}
		public String getHou_pai_du_li_kong_tiao() {
			return hou_pai_du_li_kong_tiao;
		}
		public void setHou_pai_du_li_kong_tiao(String hou_pai_du_li_kong_tiao) {
			this.hou_pai_du_li_kong_tiao = hou_pai_du_li_kong_tiao;
		}
		public String getHou_pai_chu_feng_kou() {
			return hou_pai_chu_feng_kou;
		}
		public void setHou_pai_chu_feng_kou(String hou_pai_chu_feng_kou) {
			this.hou_pai_chu_feng_kou = hou_pai_chu_feng_kou;
		}
		public String getKong_qi_tiao_jie() {
			return kong_qi_tiao_jie;
		}
		public void setKong_qi_tiao_jie(String kong_qi_tiao_jie) {
			this.kong_qi_tiao_jie = kong_qi_tiao_jie;
		}
		public String getKong_qi_jing_hua_zhuang_zhi() {
			return kong_qi_jing_hua_zhuang_zhi;
		}
		public void setKong_qi_jing_hua_zhuang_zhi(String kong_qi_jing_hua_zhuang_zhi) {
			this.kong_qi_jing_hua_zhuang_zhi = kong_qi_jing_hua_zhuang_zhi;
		}
		public String getChe_zai_bing_xiang() {
			return che_zai_bing_xiang;
		}
		public void setChe_zai_bing_xiang(String che_zai_bing_xiang) {
			this.che_zai_bing_xiang = che_zai_bing_xiang;
		}
		public String getTest3_zhu() {
			return test3_zhu;
		}
		public void setTest3_zhu(String test3_zhu) {
			this.test3_zhu = test3_zhu;
		}
		public String getTest6_zhu() {
			return test6_zhu;
		}
		public void setTest6_zhu(String test6_zhu) {
			this.test6_zhu = test6_zhu;
		}
		public String getTest7_zhu() {
			return test7_zhu;
		}
		public void setTest7_zhu(String test7_zhu) {
			this.test7_zhu = test7_zhu;
		}
		public int getZhong_yang_cha_su_qi_suo() {
			return zhong_yang_cha_su_qi_suo;
		}
		public void setZhong_yang_cha_su_qi_suo(int zhong_yang_cha_su_qi_suo) {
			this.zhong_yang_cha_su_qi_suo = zhong_yang_cha_su_qi_suo;
		}
		public float getDian_ji_zui_da_niu_ju() {
			return dian_ji_zui_da_niu_ju;
		}
		public void setDian_ji_zui_da_niu_ju(float dian_ji_zui_da_niu_ju) {
			this.dian_ji_zui_da_niu_ju = dian_ji_zui_da_niu_ju;
		}
		public float getXi_tong_dian_ya() {
			return xi_tong_dian_ya;
		}
		public void setXi_tong_dian_ya(float xi_tong_dian_ya) {
			this.xi_tong_dian_ya = xi_tong_dian_ya;
		}
		public String getDian_ji_type() {
			return dian_ji_type;
		}
		public void setDian_ji_type(String dian_ji_type) {
			this.dian_ji_type = dian_ji_type;
		}
		public float getPu_tong_chong_dian_time() {
			return pu_tong_chong_dian_time;
		}
		public void setPu_tong_chong_dian_time(float pu_tong_chong_dian_time) {
			this.pu_tong_chong_dian_time = pu_tong_chong_dian_time;
		}
		public float getKuai_su_chong_dian_time() {
			return kuai_su_chong_dian_time;
		}
		public void setKuai_su_chong_dian_time(float kuai_su_chong_dian_time) {
			this.kuai_su_chong_dian_time = kuai_su_chong_dian_time;
		}
		public float getDian_chi_dian_ya() {
			return dian_chi_dian_ya;
		}
		public void setDian_chi_dian_ya(float dian_chi_dian_ya) {
			this.dian_chi_dian_ya = dian_chi_dian_ya;
		}
		public float getDian_chi_rong_liang() {
			return dian_chi_rong_liang;
		}
		public void setDian_chi_rong_liang(float dian_chi_rong_liang) {
			this.dian_chi_rong_liang = dian_chi_rong_liang;
		}
		public String getDian_chi_type() {
			return dian_chi_type;
		}
		public void setDian_chi_type(String dian_chi_type) {
			this.dian_chi_type = dian_chi_type;
		}
		public float getBai_gong_li_hao_dian_liang() {
			return bai_gong_li_hao_dian_liang;
		}
		public void setBai_gong_li_hao_dian_liang(float bai_gong_li_hao_dian_liang) {
			this.bai_gong_li_hao_dian_liang = bai_gong_li_hao_dian_liang;
		}
		public float getChun_dian_zui_gao_xu_hang_li_cheng() {
			return chun_dian_zui_gao_xu_hang_li_cheng;
		}
		public void setChun_dian_zui_gao_xu_hang_li_cheng(
				float chun_dian_zui_gao_xu_hang_li_cheng) {
			this.chun_dian_zui_gao_xu_hang_li_cheng = chun_dian_zui_gao_xu_hang_li_cheng;
		}
		public String getGuan_fang_jia_su_shi_jian() {
			return guan_fang_jia_su_shi_jian;
		}
		public void setGuan_fang_jia_su_shi_jian(String guan_fang_jia_su_shi_jian) {
			this.guan_fang_jia_su_shi_jian = guan_fang_jia_su_shi_jian;
		}
		public String getZui_da_she_shui_shen_du() {
			return zui_da_she_shui_shen_du;
		}
		public void setZui_da_she_shui_shen_du(String zui_da_she_shui_shen_du) {
			this.zui_da_she_shui_shen_du = zui_da_she_shui_shen_du;
		}
		public String getFeng_zu_xi_shu() {
			return feng_zu_xi_shu;
		}
		public void setFeng_zu_xi_shu(String feng_zu_xi_shu) {
			this.feng_zu_xi_shu = feng_zu_xi_shu;
		}
		public int getDian_zi_dang_gan() {
			return dian_zi_dang_gan;
		}
		public void setDian_zi_dang_gan(int dian_zi_dang_gan) {
			this.dian_zi_dang_gan = dian_zi_dang_gan;
		}
		public int getAn_quan_dai_qi_nang() {
			return an_quan_dai_qi_nang;
		}
		public void setAn_quan_dai_qi_nang(int an_quan_dai_qi_nang) {
			this.an_quan_dai_qi_nang = an_quan_dai_qi_nang;
		}
		public int getDian_zi_xian_su() {
			return dian_zi_xian_su;
		}
		public void setDian_zi_xian_su(int dian_zi_xian_su) {
			this.dian_zi_xian_su = dian_zi_xian_su;
		}
		public int getKui_suo_shi_zhi_dong() {
			return kui_suo_shi_zhi_dong;
		}
		public void setKui_suo_shi_zhi_dong(int kui_suo_shi_zhi_dong) {
			this.kui_suo_shi_zhi_dong = kui_suo_shi_zhi_dong;
		}
		public int getTest13_zhu() {
			return test13_zhu;
		}
		public void setTest13_zhu(int test13_zhu) {
			this.test13_zhu = test13_zhu;
		}
		public int getMang_dian_qian_ce() {
			return mang_dian_qian_ce;
		}
		public void setMang_dian_qian_ce(int mang_dian_qian_ce) {
			this.mang_dian_qian_ce = mang_dian_qian_ce;
		}
		public int getFa_dong_ji_zu_li() {
			return fa_dong_ji_zu_li;
		}
		public void setFa_dong_ji_zu_li(int fa_dong_ji_zu_li) {
			this.fa_dong_ji_zu_li = fa_dong_ji_zu_li;
		}
		public int getWan_dao_zhi_dong_kong_zhi() {
			return wan_dao_zhi_dong_kong_zhi;
		}
		public void setWan_dao_zhi_dong_kong_zhi(int wan_dao_zhi_dong_kong_zhi) {
			this.wan_dao_zhi_dong_kong_zhi = wan_dao_zhi_dong_kong_zhi;
		}
		public int getYi_che_wang_id() {
			return yi_che_wang_id;
		}
		public void setYi_che_wang_id(int yi_che_wang_id) {
			this.yi_che_wang_id = yi_che_wang_id;
		}
		public String getYi_che_wang_image() {
			return yi_che_wang_image;
		}
		public void setYi_che_wang_image(String yi_che_wang_image) {
			this.yi_che_wang_image = yi_che_wang_image;
		}
		public String getYi_che_wang_namespell() {
			return yi_che_wang_namespell;
		}
		public void setYi_che_wang_namespell(String yi_che_wang_namespell) {
			this.yi_che_wang_namespell = yi_che_wang_namespell;
		}
		public int getYi_che_car_id() {
			return yi_che_car_id;
		}
		public void setYi_che_car_id(int yi_che_car_id) {
			this.yi_che_car_id = yi_che_car_id;
		}
		public int getCar_sale_state() {
			return car_sale_state;
		}
		public void setCar_sale_state(int car_sale_state) {
			this.car_sale_state = car_sale_state;
		}
		public int getYi_che_re_du() {
			return yi_che_re_du;
		}
		public void setYi_che_re_du(int yi_che_re_du) {
			this.yi_che_re_du = yi_che_re_du;
		}
}
