package com.demo.domain;

import java.io.Serializable;

/**
 * 汽车品牌
 * @author machao
 *
 */
public class CarPinPai implements Serializable {
	
	private static final long serialVersionUID = -5099929003717018483L;
	private int id; //品牌id
	private String name; //品牌名字
	private String nameSpell;	// 品牌拼音名称
	private String nameFenci;	// 搜索引擎需要用的分词字段
	private int level; //汽车品牌等级
	private int parentId; //顶级品牌id
	private String logoImage; //汽车品牌logo图标
	private int displayorder; //排序因子
	private int yi_che_pin_pai_id;	/* 易车网品牌ID */
	//private int hot;				//热门品牌
	
	/* 冗余字段 */
	private int nian_xian; 		//汽车年限
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNameSpell() {
		return nameSpell;
	}
	public void setNameSpell(String nameSpell) {
		this.nameSpell = nameSpell;
	}
	public String getNameFenci() {
		return nameFenci;
	}
	public void setNameFenci(String nameFenci) {
		this.nameFenci = nameFenci;
	}
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public int getParentId() {
		return parentId;
	}
	public void setParentId(int parentId) {
		this.parentId = parentId;
	}
	public String getLogoImage() {
		return logoImage;
	}
	public void setLogoImage(String logoImage) {
		this.logoImage = logoImage;
	}
	public int getDisplayorder() {
		return displayorder;
	}
	public void setDisplayorder(int displayorder) {
		this.displayorder = displayorder;
	}
	public int getYi_che_pin_pai_id() {
		return yi_che_pin_pai_id;
	}
	public void setYi_che_pin_pai_id(int yi_che_pin_pai_id) {
		this.yi_che_pin_pai_id = yi_che_pin_pai_id;
	}
	public int getNian_xian() {
		return nian_xian;
	}
	public void setNian_xian(int nian_xian) {
		this.nian_xian = nian_xian;
	}
}
