package com.demo.domain;

import java.io.Serializable;

import org.joda.time.DateTime;

/**
 * 奔溃日志对象
 */
public class CrashLog implements Serializable {
	private static final long serialVersionUID = 3671891344114886292L;
	
	/** 未解决bug */
	public static final transient int STATUS_UNDO = 0;
	/** 已经解决bug */
	public static final transient int STATUS_DO = 1;
	/** app类型:ios */
	public static final transient int TYPE_IOS = 0;
	/** app类型:android */
	public static final transient int TYPE_ANDROID = 1;
	
	private int id;
	private int app_type;		/* app类型：0表示ios版爱车通，1表示android版爱车通 */
	private String app_version;	/* app版本号 */
	private int error_code;			/* 错误码，由客户端自己定义 */
	private String device_info;		/* 设备类型：如iPhone5c，ios8.1.1，未越狱、小米3，miui3，android4.4等 */
	private String content; 		/* 日志内容 */
	private int status;				/* 状态：0未解决，1已解决 */
	private DateTime create_time;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getApp_type() {
		return app_type;
	}
	public void setApp_type(int app_type) {
		this.app_type = app_type;
	}
	public String getApp_version() {
		return app_version;
	}
	public void setApp_version(String app_version) {
		this.app_version = app_version;
	}
	public int getError_code() {
		return error_code;
	}
	public void setError_code(int error_code) {
		this.error_code = error_code;
	}
	public String getDevice_info() {
		return device_info;
	}
	public void setDevice_info(String device_info) {
		this.device_info = device_info;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public DateTime getCreate_time() {
		return create_time;
	}
	public void setCreate_time(DateTime create_time) {
		this.create_time = create_time;
	}
}
