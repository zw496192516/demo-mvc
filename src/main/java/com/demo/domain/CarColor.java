package com.demo.domain;

import java.io.Serializable;
/**
 * 汽车颜色临时对象
 * @author <a href="mailto:449735036@qq.com">FuDan</a>
 *
 * 2014-5-9
 */
public class CarColor implements Serializable{
	private static final long serialVersionUID = -3267823749293573381L;
	
	private int id;
	private int cpp_detail_id; /* 汽车系列id（对应表t_car_pin_pai_detail表） */
	private String name;	/* 颜色名称 */
	private String value;	/* 颜色值 */
	private String default_car_image;	/* 该颜色的车，对应的默认汽车图片 */
	private int image_total;				/* 该颜色车系下的图片总数*/
	private int count_of_part_wai_guan;		/* 该颜色对应的外观的图片总数*/
	private int count_of_part_nei_shi;		/* 该颜色对应的内饰的图片总数*/
	private int count_of_part_kong_jian;	/* 该颜色对应的空间图片总数*/
	
	public int getCpp_detail_id() {
		return cpp_detail_id;
	}
	public void setCpp_detail_id(int cpp_detail_id) {
		this.cpp_detail_id = cpp_detail_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getDefault_car_image() {
		return default_car_image;
	}
	public void setDefault_car_image(String default_car_image) {
		this.default_car_image = default_car_image;
	}
	public int getImage_total() {
		return image_total;
	}
	public void setImage_total(int image_total) {
		this.image_total = image_total;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getCount_of_part_wai_guan() {
		return count_of_part_wai_guan;
	}
	public void setCount_of_part_wai_guan(int count_of_part_wai_guan) {
		this.count_of_part_wai_guan = count_of_part_wai_guan;
	}
	public int getCount_of_part_nei_shi() {
		return count_of_part_nei_shi;
	}
	public void setCount_of_part_nei_shi(int count_of_part_nei_shi) {
		this.count_of_part_nei_shi = count_of_part_nei_shi;
	}
	public int getCount_of_part_kong_jian() {
		return count_of_part_kong_jian;
	}
	public void setCount_of_part_kong_jian(int count_of_part_kong_jian) {
		this.count_of_part_kong_jian = count_of_part_kong_jian;
	}
	
}
