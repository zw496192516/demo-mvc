package com.demo.domain;

import java.io.Serializable;

/**
 * 汽车搜索条件
 * @author zhuwei
 *
 */
public class CarSearchCondition implements Serializable{

	private static final long serialVersionUID = -2109377952877329898L;
	private int id;
	private int car_detail_id; 	/*汽车系列id(关联t_car_detail表)*/
	private int type_id; 		/*车型id，对应表t_car_type*/
	private int jia_ge;   		/*汽车价格*/
	private int che_shen;  		/*车身：1.两厢、2.三厢、3.旅行版*/
	private int qu_dong;  		/*驱动：1.前驱，2.后驱，四驱【3.全时四驱，4.分时四驱，5.适时四驱】*/
	private int ran_liao;   	/*燃料：1.汽油，2.柴油，3.纯电动，4.油电混合，5.油气混合*/
	private int bian_su_xiang;   	/*变速箱：1.手动，自动【2.半自动(AMT)，3.自动(AT)，4.手自一体，5.无极变速(CVT)，6.双离合(DSG)】*/
	private int che_men_shu;   	/*车门数*/
	private String zuo_wei_shu;   	/*座位数，数据可能为5或者5-7，所以由int型改为String型*/
	private int nian_xian;		/*汽车年限*/
	private float pai_liang;  	/*排量(L)*/
	private String pinpai_desc;					/*品牌描述*/
	private int image_total;					/*该车系下的图片总数*/
	private int count_of_part_wai_guan;			/*该车对应的外观的图片总数*/
	private int count_of_part_nei_shi;			/*该车对应的内饰的图片总数*/
	private int count_of_part_kong_jian;		/*该车对应的空间图片总数*/
	private int merchants_min;		/*商家报价的最小值 */			
	private int merchants_max;		/*商家报价的最大值 */
	private int click_count;		/*点击数*/
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getCar_detail_id() {
		return car_detail_id;
	}
	public void setCar_detail_id(int car_detail_id) {
		this.car_detail_id = car_detail_id;
	}
	public int getType_id() {
		return type_id;
	}
	public void setType_id(int type_id) {
		this.type_id = type_id;
	}
	public int getJia_ge() {
		return jia_ge;
	}
	public void setJia_ge(int jia_ge) {
		this.jia_ge = jia_ge;
	}
	public int getChe_shen() {
		return che_shen;
	}
	public void setChe_shen(int che_shen) {
		this.che_shen = che_shen;
	}
	public int getQu_dong() {
		return qu_dong;
	}
	public void setQu_dong(int qu_dong) {
		this.qu_dong = qu_dong;
	}
	public int getRan_liao() {
		return ran_liao;
	}
	public void setRan_liao(int ran_liao) {
		this.ran_liao = ran_liao;
	}
	public int getBian_su_xiang() {
		return bian_su_xiang;
	}
	public void setBian_su_xiang(int bian_su_xiang) {
		this.bian_su_xiang = bian_su_xiang;
	}
	public int getChe_men_shu() {
		return che_men_shu;
	}
	public void setChe_men_shu(int che_men_shu) {
		this.che_men_shu = che_men_shu;
	}
	public String getZuo_wei_shu() {
		return zuo_wei_shu;
	}
	public void setZuo_wei_shu(String zuo_wei_shu) {
		this.zuo_wei_shu = zuo_wei_shu;
	}
	public int getNian_xian() {
		return nian_xian;
	}
	public void setNian_xian(int nian_xian) {
		this.nian_xian = nian_xian;
	}
	public float getPai_liang() {
		return pai_liang;
	}
	public void setPai_liang(float pai_liang) {
		this.pai_liang = pai_liang;
	}
	public String getPinpai_desc() {
		return pinpai_desc;
	}
	public void setPinpai_desc(String pinpai_desc) {
		this.pinpai_desc = pinpai_desc;
	}
	public int getImage_total() {
		return image_total;
	}
	public void setImage_total(int image_total) {
		this.image_total = image_total;
	}
	public int getCount_of_part_wai_guan() {
		return count_of_part_wai_guan;
	}
	public void setCount_of_part_wai_guan(int count_of_part_wai_guan) {
		this.count_of_part_wai_guan = count_of_part_wai_guan;
	}
	public int getCount_of_part_nei_shi() {
		return count_of_part_nei_shi;
	}
	public void setCount_of_part_nei_shi(int count_of_part_nei_shi) {
		this.count_of_part_nei_shi = count_of_part_nei_shi;
	}
	public int getCount_of_part_kong_jian() {
		return count_of_part_kong_jian;
	}
	public void setCount_of_part_kong_jian(int count_of_part_kong_jian) {
		this.count_of_part_kong_jian = count_of_part_kong_jian;
	}
	public int getMerchants_min() {
		return merchants_min;
	}
	public void setMerchants_min(int merchants_min) {
		this.merchants_min = merchants_min;
	}
	public int getMerchants_max() {
		return merchants_max;
	}
	public void setMerchants_max(int merchants_max) {
		this.merchants_max = merchants_max;
	}
	public int getClick_count() {
		return click_count;
	}
	public void setClick_count(int click_count) {
		this.click_count = click_count;
	}
}
