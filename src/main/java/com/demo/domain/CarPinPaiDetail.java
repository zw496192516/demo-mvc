package com.demo.domain;

import java.io.Serializable;

/**
 * 汽车品牌详细
 * @author machao
 *
 */
public class CarPinPaiDetail implements Serializable {
	private static final long serialVersionUID = -4772590257322697233L;
	
	private int id; 				//汽车品牌详细编号
	private String alias_name;		//名称
	private String name_fenci;		//名称英文分词
	private String cover_photo;		//封面图
	private String dealer_price;	//报价区间
	private String spelling;		//拼写
	private String picture;			//效果图
	private int country;			//国别（中国 = 8,日本 = 9,德国 = 10,美国 = 11,韩国 = 12,法国 = 13,英国 = 14,意大利 = 15,其他 16
	private int country_type;		//国家类型 1. 自主 2. 合资 3. 进口
	private String displacement;	//排量
	private int level;				//级别
	private int serial_id;			//子品牌编号
	private int image_total;					/*该系列车下的图片总数（不包含t_pin_pai_image表的图片数据）*/
	private int count_of_part_tu_jie;			/*该车对应的图解图片总数*/
	private int count_of_part_guan_fang_color;	/*该车对应的官方车体颜色图总数*/
	private int count_of_part_guan_fang_detail;	/*该车对应的官方详细图*/
	private int count_of_part_che_zhan;			/*该车对应的车展图片总数*/
	private int yi_che_pin_pai_id;              /* 易车子品牌id */
	private int code; //更新标识，每次更新都加1
	private int article_count; //该车系文章数
	private int review_count; //汽车点评数
	private int question_count; //问答帖子数
	private float wai_guan_score; //外观评分
	private float nei_shi_score; //内饰评分
	private float kong_jian_score; //空间评分
	private float dong_li_score; //动力评分
	private float cao_kong_score; //操控评分
	private float pei_zhi_score; //配置评分
	private float shu_shi_du_score; //舒适度评分
	private float xing_jia_bi_score; //性价比评分
	private float zong_he_score;//综合评分
	private int click_count; 	//点击数
	private String enname;		//易车英文名
	private int rank;			//所有车型排行
	private int che_shen;		//车身：1.两厢、2.三厢、3.旅行版
	private int jiang_jia_max;	//根据车款（jiage - merchants_min）计算的最高降价
	private int commission;		//顾问佣金
	private int follow_count;	//当前用户关注总数
	private String follow_head; //默认头像的集合（3个）
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAlias_name() {
		return alias_name;
	}
	public void setAlias_name(String alias_name) {
		this.alias_name = alias_name;
	}
	public String getName_fenci() {
		return name_fenci;
	}
	public void setName_fenci(String name_fenci) {
		this.name_fenci = name_fenci;
	}
	public String getCover_photo() {
		return cover_photo;
	}
	public void setCover_photo(String cover_photo) {
		this.cover_photo = cover_photo;
	}
	public String getDealer_price() {
		return dealer_price;
	}
	public void setDealer_price(String dealer_price) {
		this.dealer_price = dealer_price;
	}
	public String getSpelling() {
		return spelling;
	}
	public void setSpelling(String spelling) {
		this.spelling = spelling;
	}
	public String getPicture() {
		return picture;
	}
	public void setPicture(String picture) {
		this.picture = picture;
	}
	public int getCountry() {
		return country;
	}
	public void setCountry(int country) {
		this.country = country;
	}
	public String getDisplacement() {
		return displacement;
	}
	public void setDisplacement(String displacement) {
		this.displacement = displacement;
	}
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public int getSerial_id() {
		return serial_id;
	}
	public void setSerial_id(int serial_id) {
		this.serial_id = serial_id;
	}
	public int getImage_total() {
		return image_total;
	}
	public void setImage_total(int image_total) {
		this.image_total = image_total;
	}
	public int getCount_of_part_tu_jie() {
		return count_of_part_tu_jie;
	}
	public void setCount_of_part_tu_jie(int count_of_part_tu_jie) {
		this.count_of_part_tu_jie = count_of_part_tu_jie;
	}
	public int getCount_of_part_guan_fang_color() {
		return count_of_part_guan_fang_color;
	}
	public void setCount_of_part_guan_fang_color(int count_of_part_guan_fang_color) {
		this.count_of_part_guan_fang_color = count_of_part_guan_fang_color;
	}
	public int getCount_of_part_guan_fang_detail() {
		return count_of_part_guan_fang_detail;
	}
	public void setCount_of_part_guan_fang_detail(int count_of_part_guan_fang_detail) {
		this.count_of_part_guan_fang_detail = count_of_part_guan_fang_detail;
	}
	public int getCount_of_part_che_zhan() {
		return count_of_part_che_zhan;
	}
	public void setCount_of_part_che_zhan(int count_of_part_che_zhan) {
		this.count_of_part_che_zhan = count_of_part_che_zhan;
	}
	public int getYi_che_pin_pai_id() {
		return yi_che_pin_pai_id;
	}
	public void setYi_che_pin_pai_id(int yi_che_pin_pai_id) {
		this.yi_che_pin_pai_id = yi_che_pin_pai_id;
	}
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public int getArticle_count() {
		return article_count;
	}
	public void setArticle_count(int article_count) {
		this.article_count = article_count;
	}
	public int getReview_count() {
		return review_count;
	}
	public void setReview_count(int review_count) {
		this.review_count = review_count;
	}
	public int getQuestion_count() {
		return question_count;
	}
	public void setQuestion_count(int question_count) {
		this.question_count = question_count;
	}
	public float getWai_guan_score() {
		return wai_guan_score;
	}
	public void setWai_guan_score(float wai_guan_score) {
		this.wai_guan_score = wai_guan_score;
	}
	public float getNei_shi_score() {
		return nei_shi_score;
	}
	public void setNei_shi_score(float nei_shi_score) {
		this.nei_shi_score = nei_shi_score;
	}
	public float getKong_jian_score() {
		return kong_jian_score;
	}
	public void setKong_jian_score(float kong_jian_score) {
		this.kong_jian_score = kong_jian_score;
	}
	public float getDong_li_score() {
		return dong_li_score;
	}
	public void setDong_li_score(float dong_li_score) {
		this.dong_li_score = dong_li_score;
	}
	public float getCao_kong_score() {
		return cao_kong_score;
	}
	public void setCao_kong_score(float cao_kong_score) {
		this.cao_kong_score = cao_kong_score;
	}
	public float getPei_zhi_score() {
		return pei_zhi_score;
	}
	public void setPei_zhi_score(float pei_zhi_score) {
		this.pei_zhi_score = pei_zhi_score;
	}
	public float getShu_shi_du_score() {
		return shu_shi_du_score;
	}
	public void setShu_shi_du_score(float shu_shi_du_score) {
		this.shu_shi_du_score = shu_shi_du_score;
	}
	public float getXing_jia_bi_score() {
		return xing_jia_bi_score;
	}
	public void setXing_jia_bi_score(float xing_jia_bi_score) {
		this.xing_jia_bi_score = xing_jia_bi_score;
	}
	public void setZong_he_score(float zong_he_score) {
		this.zong_he_score = zong_he_score;
	}
	public float getZong_he_score() {
		return zong_he_score;
	}
	public int getClick_count() {
		return click_count;
	}
	public void setClick_count(int click_count) {
		this.click_count = click_count;
	}
	public String getEnname() {
		return enname;
	}
	public void setEnname(String enname) {
		this.enname = enname;
	}
	public int getRank() {
		return rank;
	}
	public void setRank(int rank) {
		this.rank = rank;
	}
	public int getChe_shen() {
		return che_shen;
	}
	public void setChe_shen(int che_shen) {
		this.che_shen = che_shen;
	}
	public int getJiang_jia_max() {
		return jiang_jia_max;
	}
	public void setJiang_jia_max(int jiang_jia_max) {
		this.jiang_jia_max = jiang_jia_max;
	}
	public int getCountry_type() {
		return country_type;
	}
	public void setCountry_type(int country_type) {
		this.country_type = country_type;
	}
	public int getCommission() {
		return commission;
	}
	public void setCommission(int commission) {
		this.commission = commission;
	}
	public int getFollow_count() {
		return follow_count;
	}
	public void setFollow_count(int follow_count) {
		this.follow_count = follow_count;
	}
	public String getFollow_head() {
		return follow_head;
	}
	public void setFollow_head(String follow_head) {
		this.follow_head = follow_head;
	}
}
